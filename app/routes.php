<?php

use dwes\core\App;

try {
    $router = App::get('router');
} catch (\dwes\app\exceptions\AppException $e) {

}

$router->post('home','RestaurantController@listar');
$router->get('home', 'RestaurantController@listar');
$router->get("", 'RestaurantController@listar');
$router->get('add-restaurant', 'RestaurantController@addRestaurant','ROLE_USER');
$router->post('restaurant/new','RestaurantController@newRestaurant','ROLE_USER');
$router->get('delete-restaurant/:id','RestaurantController@deleteRestaurant','ROLE_USER');
$router->get('edit-restaurant/:id','RestaurantController@editRestaurant','ROLE_USER');
$router->post('restaurant/update/:id','RestaurantController@updateRestaurant','ROLE_USER');

$router->get('restaurant-details/:id','RestaurantController@showRestaurant','ROLE_USER');
$router->get('profile/:id','UserController@show_profile','ROLE_USER');
$router->get('profilepage/:id','UserController@show_profile1','ROLE_USER');

$router->get('edit-profile/:id','UserController@edit_profile','ROLE_USER');
$router->post('edit-profile/formName/:id','UserController@changeNameEmail','ROLE_USER');
$router->post('edit-profile/formAvatar/:id','UserController@changeAvatar','ROLE_USER');
$router->post('edit-profile/formPassword/:id','UserController@changePassword','ROLE_USER');
$router->post('edit-profile/changeLanguaje/:id','UserController@changeLanguaje');

$router->get('login','AuthController@login');
$router->post('check-login','AuthController@checkLogin');
$router->get('logout','AuthController@logout','ROLE_USER');

$router->get('register','AuthController@registro');
$router->post('check-registro','AuthController@checkRegistro');

$router->post('addCommentRestaurant/:id','RestaurantController@addCommentToRestaurante');
$router->get('about','PageController@about');

$router->get('idioma/:idioma', 'InternacionalizationController@cambiaIdioma', 'ROLE_ANONYMUS');
$router->post('new-message/:id','MessageController@newMessage');
$router->post('new-message','MessageController@newMessageWithEmail');
$router->get('message/:id','MessageController@showMessage');
$router->get('deleteMessage','MessageController@deleteMessage');
$router->get('message_send/:id','MessageController@listMessageSendByMe');
$router->get('detail_message','MessageController@showDetailMessage');
$router->get('show-sendMessage/:id','MessageController@showSendMessage');
$router->post('show-sendMessage/:id','MessageController@showSendMessage');
$router->get('list_user','UserController@listAlluser');