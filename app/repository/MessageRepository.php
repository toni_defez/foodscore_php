<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 1/01/19
 * Time: 19:10
 */

namespace dwes\app\repository;


use dwes\app\entity\Message;
use dwes\app\exceptions\QueryException;
use dwes\core\App;
use dwes\core\database\QueryBuilder;
use SQLiteException;

class MessageRepository extends QueryBuilder
{
    public function __construct(string $table='mensaje',string $classEntity=Message::class)
    {
        parent::__construct($table,$classEntity);
    }

    public function getAllMessageByUsuario($id){
        //ESTOS SON LOS MENSAJES RECIBIDOS

        $arrayResult=[];
        try {
            $sql = "select mensaje.titulo,mensaje.id,mensaje.fecha_hora, usuario.name from mensaje,usuario where usuario.id "
                . "= mensaje.remitente_id  and mensaje.destinatario_id=".$id;
            $arrayResult= $this->executeQueryArray($sql);
        }
        catch (\Exception $e){
            echo $e->getMessage();
        }


        return $arrayResult;
    }

    /**
     * @param $id
     * @param $idUsuario
     * @return mixed
     * @throws \dwes\app\exceptions\QueryException
     */
    public function getMessageById($id,$idUsuario,$admin =false){

        $sql = "SELECT mensaje.id,mensaje.titulo,mensaje.mensaje,mensaje.fecha_hora,usu1.email as remitente ,"
            ."usu2.email as destinatario FROM `mensaje`, usuario as usu1 , usuario as usu2 WHERE usu1.id = mensaje.remitente_id ".
            "and usu2.id = mensaje.destinatario_id and mensaje.id = $id ";
        if(!$admin){
            $sql.= " and (usu2.id = $idUsuario or usu1.id = $idUsuario) ";
        }
        $sql.= "ORDER BY `mensaje`.`fecha_hora` ASC";

        $arrayResult= $this->executeQueryArray($sql);
        if(empty($arrayResult))
            throw  new QueryException("No existe ese correo");

        return $arrayResult[0];
    }


    public function getAllMessageSentByUsuario($id){

        try {
            $sql = "select mensaje.titulo,mensaje.id,mensaje.fecha_hora, usuario.name from mensaje,usuario where usuario.id "
                . "= mensaje.remitente_id  and mensaje.remitente_id=".$id;
        }
        catch (\Exception $e){
            echo $e->getMessage();
        }
        $arrayResult= $this->executeQueryArray($sql);

        return $arrayResult;
    }

}