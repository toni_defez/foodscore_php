<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 9/12/18
 * Time: 16:48
 */

namespace dwes\app\repository;

use dwes\core\database\QueryBuilder;
use dwes\app\entity\Categoria;
use dwes\app\entity\Restaurant;

class CategoriaRepository extends QueryBuilder
{
    public function __construct(string $table='categoria',string $classEntity=Categoria::class)
    {
        parent::__construct($table,$classEntity);
    }

    /**
     * @param Categoria $categoria
     * @throws \dwes\exceptions\QueryException
     */
    public function nuevoRestaurante(Categoria $categoria){
        $categoria->setNumRestaurante($categoria->getNumRestaurante()+1);
        $this->update($categoria);
    }

    /**
     * @param Categoria $categoria
     * @throws \dwes\exceptions\QueryException
     */
    public function  borradoRestaurante(Categoria $categoria){
        $categoria->setNumRestaurante($categoria->getNumRestaurante()-1);
        $this->update($categoria);
    }


}