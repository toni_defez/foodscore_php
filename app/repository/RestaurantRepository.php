<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 12/11/18
 * Time: 22:25
 */
namespace dwes\app\repository;

use dwes\app\entity\Comment;
use dwes\app\exceptions\QueryException;
use dwes\core\App;
use dwes\core\database\QueryBuilder;
use dwes\app\entity\Categoria;
use dwes\app\entity\Restaurant;
use dwes\app\exceptions\AppException;


class RestaurantRepository extends QueryBuilder
{

    /**
     * RestaurantRepository constructor.
     */
    public function __construct(string $table='restaurant',string $classEntity=Restaurant::class)
    {
        parent::__construct($table,$classEntity);
    }

    /**
     * @param Restaurant $restaurant
     * @return Categoria
     * @throws \dwes\exceptions\QueryException
     */
    public function getCategoria (Restaurant $restaurant):Categoria
    {
        try {
            $categoriaRepositorio = new CategoriaRepository();
            return $categoriaRepositorio->find($restaurant->getCategoria());
        } catch (AppException $e) {
        }

    }

    /**
     * @param Restaurant $restaurant
     * @throws \dwes\exceptions\QueryException
     */
    public function  guarda( Restaurant $restaurant){

        $fnGuardaRestaurante = function () use ($restaurant){

            $categoria = $this->getCategoria($restaurant);
            App::getRepository(CategoriaRepository::class)->nuevoRestaurante($categoria);
            App::getRepository(UserRepository::class)->updateNumRestaurants($restaurant->getuserId(),1);
            $this->save($restaurant);
        };

        $this->executeTransaction($fnGuardaRestaurante);
    }

    /**
     * @param $idImagen
     * @throws \dwes\exceptions\QueryException
     */
    public function deleteRestaurant($idImagen)
    {
        $fnBorrarRestaurante = function () use ($idImagen){
            $restaurant= App::getRepository(RestaurantRepository::class)->find($idImagen);
            $restaurant->setActive(false);
            $categoria = $this->getCategoria($restaurant);
            App::getRepository(CategoriaRepository::class)->borradoRestaurante($categoria);
            App::getRepository(UserRepository::class)->updateNumRestaurants($restaurant->getuserId(),-1);
            $this->update($restaurant);
        };
        $this->executeTransaction($fnBorrarRestaurante);
    }


    public function updateRatingRestaurant(int $idRestaurante, Comment $comment ){
        $fnUpdateRestaurente = function () use ($idRestaurante, $comment){
            $restaurant = $this->find($idRestaurante);
            App::getRepository(CommentsRepository::class)->save($comment);
            $currentRating = $this->getCurrentRating($idRestaurante);
            $restaurant->setNumberStar($currentRating);
            $this->update($restaurant);
        };
        try {
            $this->executeTransaction($fnUpdateRestaurente);
        } catch (QueryException $e) {
            echo $e->getMessage();
        }
    }

    public function getCurrentRating($idRestaurante) {
        $select = "Select ROUND(avg(rating)) as rating from comment where id_restaurante =".$idRestaurante;
        return $this->executeQueryArray($select)[0]['rating'];
    }
}