<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 11/12/18
 * Time: 1:29
 */

namespace dwes\app\repository;


use dwes\app\entity\Usuario;
use dwes\core\App;
use dwes\core\database\QueryBuilder;

class UserRepository extends QueryBuilder
{

    public function __construct(string $table='usuario',string $classEntity=Usuario::class)
    {
        parent::__construct($table,$classEntity);
    }


    public function updateNumRestaurants($id,$num){
        $user = $this->find($id);
        $user->setNumRestaurants(($user->getNumRestaurants()+$num));
        $this->update($user);
    }
}