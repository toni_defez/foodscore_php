<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 11/12/18
 * Time: 1:08
 */

namespace dwes\app\repository;


use dwes\app\entity\Comment;
use dwes\app\entity\Restaurant;
use dwes\app\entity\Usuario;
use dwes\core\database\QueryBuilder;

class CommentsRepository extends QueryBuilder
{

    public function __construct(string $table='comment',string $classEntity=Comment::class)
    {
        parent::__construct($table,$classEntity);
    }

    public function  getAllCommentsByRestaurante(int $idRestaurante){

        $sql = "select usuario.id as idUsuario,usuario.name as nameUsuario,usuario.avatar, ".
            "comment.id as IdComentario,comment.rating,comment.text,comment.rating ,comment.fecha ".
            "from comment,usuario where usuario.id = comment.id_usuario and comment.id_restaurante = $idRestaurante";

        $arrayResult= $this->executeQueryArray($sql);

        foreach($arrayResult as $indice=>$usuario)
        {
            foreach ( $usuario as $key=>$value){
                if($key === 'avatar'){
                    $arrayResult[$indice][$key] = Usuario::RUTA_IMAGENES_USUARIOS."min_".$value;
                }
            }
        }
        return $arrayResult;
    }

    /**
     * @param int $idUsuario
     * @return array
     * @throws \dwes\app\exceptions\QueryException
     */
    public function getAllCommentsByUsuario(int $idUsuario){

        $sql = "SELECT comment.id_restaurante,comment.rating,comment.text, restaurant.imagen , restaurant.name FROM `comment`,`restaurant`
          WHERE comment.id_restaurante = restaurant.id and comment.id_usuario = $idUsuario limit 3 ";

        $arrayResult= $this->executeQueryArray($sql);

        foreach($arrayResult as $indice=>$comentario)
        {
            foreach ( $comentario as $key=>$value){
                if($key === 'imagen'){
                    $arrayResult[$indice][$key] = Restaurant::RUTA_IMAGENES_PORTAFOLIO."min_".$value;
                }
            }
        }
        return $arrayResult;

    }

}