<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 12/11/18
 * Time: 20:31
 */

return [
  'database'=>[
      'name' => 'dbRestaurante',
      'username' => 'debian1',
      'password' => 'debian',
      'connection' =>'mysql:host=dwes.local',
      'options' => [
          PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
          PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION,
          PDO::ATTR_PERSISTENT => true
      ]
  ],
    'logger'=>[
        'name'=>'log-dwes',
        'file'=>'logs/dwes.log'
    ],
    'mailer'=>[
        'host'=>'smtp.gmail.com',
        'port'=>465,
        'username'=>'toni.daw.defez@gmail.com',
        'password'=>'Soigenial123',
        'encryption'=>'ssl',
        'alias'=>'foodScore'
    ],
    'proyect'=>[
        'namespaces'=>'dwes'
    ],
    'security'=>[
        'roles'=>[
            'ROLE_ADMIN'=>3,
            'ROLE_USER'=>2,
            'ROLE_ANONYMUS'=>1
        ]
    ],
    'settings'=>[
        'pagination'=>5
    ]
];