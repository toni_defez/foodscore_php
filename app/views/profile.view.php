

  <div class="container">
    <div id="profile">
      <div class="row mt-4">
        <div class="col-3">
          <img class="w-100" id="avatar"  src='/<?=$usuario->getUrlAvatar() ?>' alt="">
        </div>
        <div class="col-9">
          <h4><?=$usuario->getName() ?></h4>
          <h4>
            <small class="text-muted"><?=$usuario->getEmail() ?></small>
          </h4>
            <p>   <small class="text-muted"><?=_("Propietario de ") ?> <?=$usuario->getNumRestaurants() ?> <?=_("restaurantes") ?></small></p>
            <?php if ($soyYo === true || $app['user']->getRol() === 'ROLE_ADMIN'):?>
            <p><a href="/edit-profile/<?= $usuario->getId()?>" class="btn btn-primary"><?= _("Editar perfil")?></a> </p>
            <p><a href="/message/<?= $usuario->getId()?>" class="btn btn-primary"><?= _("Ver mensajes")?></a> </p>

            <?else:?>
                <?php include __DIR__ . '/partials/form_message.part.php'; ?>
            <?endif;?>
        </div>
          <div class="container ">
              <h3 class="text-success" ><?= _("Restaurantes")?></h3>
                  <?php include __DIR__ . '/partials/lista_restaurantes.php'; ?>
              <? if(count($restaurants)>5):?>
                 <?php include __DIR__ . '/partials/pagination.part.php'; ?>
              <?endif;?>
          </div>

          <? if(count($comments)!=0):?>
              <ul class="list-group mt-12 mb-12" id="comments">
                  <li class="list-group-item active"><?= _("Opiniones recientes") ?></li>
                  <?php foreach ( ($comments ?? []) as $comment) :?>

                      <li class="list-group-item d-flex flex-row">
                          <a href="/restaurant-details/<?=$comment['id_restaurante'] ?>">
                              <div>
                                  <img class="mr-3" style="width: 40px;" src="/<?= $comment['imagen']?>" alt="">
                              </div>
                          </a>
                          <div>
                                  <div><strong><?=$comment['name']?></strong>
                                  <?=$comment['text'] ?>
                              </div>
                              <div>
                                  <?for ($i = 0; $i < $comment['rating']; $i++) {
                                      echo "<i class=\"fas fa-star\"></i>";
                                  }?>

                              </div>
                          </div>
                      </li>
                  <?endforeach;?>
              </ul>
          <? endif;?>
      </div>
    </div>
    <div class="mt-4" id="map"></div>
  </div>

