
<div class="container card bg-light top-buffer">

    <?php if (!empty($mensaje) || !empty($errores)) :?>

        <?php include __DIR__ . '/partials/show-error.part.php'; ?>

    <?endif; ?>


    <div class="row .mt-20">
        <h1><?= _("Mensaje de ").$nameUser?></h1>
    </div>
    <div class="row .mt-20">
        <?php include __DIR__ . '/partials/message_tab.part.php'; ?>
        <div class="col-sm-10">
            <table class="table table-inbox table-hover">
                <tbody>
                <? foreach ($messages ??[] as $message):?>
                <tr class="asdfasdf ">
                    <td class="view-message dont-show"><?=$message['name'] ?> </td>
                    <td class="view-message"><?=$message['titulo'] ?></td>
                    <td class="view-message text-right"><?=$message['fecha_hora'] ?></td>
                    <td class="view-message text-right"><a href="/detail_message?id=<?=$id?>&idmessage=<?= $message['id']?>"><?=
                    _("Ver mensaje")?></a></td>
                    <td class="view-message text-right"><a href="/deleteMessage?id=<?=$id?>&idmessage=<?= $message['id']?>">X</a></td>
                </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>
    </div>



</div>