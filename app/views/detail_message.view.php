
<div class="container card bg-light top-buffer">

    <div class="row .mt-20">
    <?php include __DIR__ . '/partials/message_tab.part.php'; ?>
        <div class="col-sm-10">
            <div class="card">
                <div class="card-header">
                    <div class="form-group">
                        <label for="correo"><?=_("Remitente")?>:</label>
                        <input type="email" class="form-control" value="<?=$message['remitente'] ?>" name="email" readonly  id="email"
                               placeholder="<?=_("Email")?>>
                        <label for="name"><?=_("Destinatario")?>:</label>
                        <input type="text" class="form-control" name="nameUser" value="<?=$message['destinatario'] ?>" readonly id="name"
                               placeholder=<?=_("Name")?>>
                        <label for="name"><?= _("Titulo")?>:</label>
                        <input type="text" class="form-control" name="nameUser" value="<?= $message['titulo']?>" readonly id="titulo"
                               placeholder="<?= _("Introduzca aqui su mensaje")?>">
                    </div>
                </div>
                <div class="card-body">
                    <p class="card-text"><?=$message['mensaje'] ?></p>
                </div>
                <? if(!$soyYo &&  $app['user']!="" && $app['user']->getId()===$id): ?>
                    <div class="card-footer">
                        <form action="/show-sendMessage/<?=$id ?>" method="post">
                            <input type="text" class="form-control  d-none" id="destinatario" name="destinatario"
                                   value="<?=$message['remitente'] ?>">
                            <input type="text" class="form-control  d-none" id="titulo" name="titulo"
                                   value="<?='Respuesta: '.$message['titulo'] ?>">
                            <button type="submit" class="btn btn-primary"><?=_("Contestar") ?></button>
                        </form>
                    </div>
                <?endif;?>
            </div>
        </div>
    </div>


</div>