
<div class="container">
<div id="placesContainer" class="card-columns mt-3">


     <?php foreach ( ($usersList ?? []) as $user) :?>

       <div class="card">
        <img class="card-img-top" src='/<?= $user->getUrlMiniaturaAvatar() ?>'>
        <div class="card-body">
          <a href="/profile/<?=$user->getId() ?>"> <h5 class="card-title"><?= $user->getName()?></h5></a>
          <p class="card-text"> Email :<?= $user->getEmail()?></p>
          <div class="card-text">
              <p>Total restaurants: <?=$user->getNumRestaurants() ?></p>
          </div>

        </div>

      </div>

    <?endforeach;?>

  </div>

</div>


