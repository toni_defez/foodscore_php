


<?php if (!empty($mensaje) || !empty($errores)) :?>

    <?php include __DIR__ . '/partials/show-error.part.php'; ?>

<?endif; ?>
  <div class="container">
    <form  id="form-profile" action="/edit-profile/formName/<?=$usuario->getId() ?>"
           method="post" novalidate class="mt-4"  role="form">
      <legend><?=_("Editar perfil") ?></legend>
      <div class="form-group">
        <label for="correo">Email:</label>
        <input type="email" class="form-control" value="<?=$usuario->getEmail()?? ''?>" name="email" id="email"
               placeholder="Email">
        <label for="name">Name:</label>
        <input type="text" class="form-control" name="nameUser"
               value="<?=$usuario->getName()?? ''?>" id="name"
               placeholder="<?=_("Nombre") ?>">
      </div>

      <button type="submit" class="btn btn-primary"> <?=_("Editar perfil") ?></button>
    </form>


    <form  id="form-avatar" action="/edit-profile/formAvatar/<?=$usuario->getId()?>" class="mt-4" method="post" novalidate
           enctype="multipart/form-data" role="form">
      <legend><?=_("Editar perfil") ?></legend>
      <div class="form-group">
          <input type="file" class="form-control" id="image" name="image">
      </div>
      <div class="row">
        <div class="col" class="w-100">
            <img class="w-100" id="avatar" src="/<?=$usuario->getUrlAvatar()?>" alt="">
        </div>
        <div class="col">
            <img class="d-none w-100" src="" alt="" id="imgPreview">
        </div>
      </div>

      <button type="submit" class="btn btn-primary"><?=_("Cambiar foto de perfil") ?></button>
    </form>


    <form  id="form-password" action="/edit-profile/formPassword/<?=$usuario->getId()?>"
           novalidate class="mt-4" method="POST" role="form">
      <legend><?=_("Cambiar contrase&ntilde;a") ?></legend>

      <div class="form-group">
        <label for="password"><?=_("Contrase&ntildea") ?>:</label>
        <input type="password" class="form-control" name="password" id="password" placeholder="<?=_("Contrase&ntilde;a") ?>">
        <label for="password2">Repeat password:</label>
        <input type="password" class="form-control" name="password2" id="password2"
               placeholder="<?=_("Repite la contrase&ntilde;a") ?>">
      </div>
      <p class="text-danger" id="errorInfo3"></p>
      <p class="text-success" id="okInfo3"></p>
      <button type="submit" class="btn btn-primary"><?=
          _("Cambiar  contrase&ntilde;a")?></button>
    </form>

      <form  id="form-languaje" action="/edit-profile/changeLanguaje/<?=$usuario->getId()?>"   novalidate class="mt-4" method="POST" role="form">
          <label for="languaje">Languaje:</label>
          <select   class="form-control" name="languaje" id="languaje" >
              <option  value="es_ES"   <?=("es_ES"=== $usuario->getIdioma()) ? 'selected':''?> ><?=_("Espa&ntilde;ol")?></option>
              <option  value="en_GB"   <?=("en_GB"=== $usuario->getIdioma()) ? 'selected':''?> >Ingles</option>
          </select>
          <button type="submit" class="btn btn-primary">Edit password</button>
      </form>
  </div>

<script src="/js/edit-profile.js"></script>


