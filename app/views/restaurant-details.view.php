

<?php include __DIR__. '/partials/show-error.part.php' ?>

 <div class="container">
    <div id="cardContainer" class="mt-4">
        <img class="card-img-top" src='/<?= $restaurant->getUrlPortafolio() ?>'>
        <div class="card-body">
            <h5 class="card-title"><?= $restaurant->getName()?></h5>
            <p class="card-text"><?= $restaurant->getDescription()?></p>
            <div class="card-text">
                <small class="text-muted">
                    <strong>Opens: </strong> <?=$restaurant->getOpenDays()?>
                </small>
                <? if ($restaurant->isOpen()) :?>
                    <span class="badge badge-success"><?= _("Abierto")?></span>
                <?else:?>
                    <span class="badge badge-danger"><?= _("Cerrado")?></span>
                <? endif;?>
            </div>
            <div class="card-text">
                <small class="text-muted">
                    <strong><?=_("Tel&eacute;fono")?>: </strong> <?= $restaurant->getTelephone() ?>
                </small>
            </div>
            <div class="card-text">
                <small class="text-muted">
                    <strong><?= _("Puntaci&oacute;n actual")?>: </strong> <?= $restaurant->getNumberStar() ?>
                </small>
            </div>
            <? if ($restaurant->isMine()  || ( $app['user']!="" && $app['user']->getRol() ?? "" === 'ROLE_ADMIN')):?>
                <button class="mt-2 btn btn-sm btn-danger btn-delete" id="<?= $restaurant->getId() ?>">
                    <?= _("Borrar")?></button>
                <a href="/edit-restaurant/<?=$restaurant->getId()?>">
                    <button class="mt-2 btn btn-sm btn-info btn-info"><?= _("Editar")?></button>
                </a>

            <?endif;?>
            </button>
        </div>
        <div class="card-footer">
            <small class="text-muted">
                <?= $restaurant->getCuisine()?>
            </small>
        </div>

    </div>

    <ul class="list-group mt-4 mb-4" id="comments">
      <li class="list-group-item active"><?=_("Comentarios recientes") ?></li>
        <?php foreach ( ($comments ?? []) as $comment) :?>
              <li class="list-group-item d-flex flex-row">
                  <a href="/profile/<?=$comment['idUsuario'] ?>">
                    <div>
                      <img class="rounded-circle mr-3" style="width: 40px;" src="/<?= $comment['avatar']?>" alt="">
                    </div>
                  </a>
                <div>
                  <div><strong><?=$comment['nameUsuario']?>
                      </strong>
                      <?=$comment['text'] ?>
                  </div>
                  <div>
                      <?for ($i = 0; $i < $comment['rating']; $i++) {
                        echo "<i class=\"fas fa-star\"></i>";
                      }?>

                  </div>
                  <small><?= $comment['fecha']?></small>
                </div>
              </li>
        <?endforeach;?>
    </ul>


    <form class="mt-4" id="commentForm" method="POST" role="form"  action="/addCommentRestaurant/<?= $restaurant->getId() ?>">
      <h4><?= _("Escribe acerca de este restaurante") ?>:</h4>
      <div class="form-group">
        <textarea class="form-control" name="comment" id="comment" placeholder="<?=_("Escribe una opinion") ?>"></textarea>
      </div>
        <input class="d-none" id="rating" type="number" name="rating" min="1" max="5">
      <div id="stars">
        <i class="far fa-star" data-score="1"></i>
        <i class="far fa-star" data-score="2"></i>
        <i class="far fa-star" data-score="3"></i>
        <i class="far fa-star" data-score="4"></i>
        <i class="far fa-star" data-score="5"></i>
      </div>

      <button type="submit" class="btn btn-primary mt-3"><?=_("Valorar") ?></button>
    </form>
   <script src="/js/detailsRestaurant.js"></script>