



<?php if (!empty($mensaje) || !empty($errores)) :?>

    <?php include __DIR__ . '/partials/show-error.part.php'; ?>

<?endif; ?>



  <div class="container">

    <form  class="form-horizontal mt-4"  action=<?= !$update ?"/restaurant/new ":"/restaurant/update/".$restaurant->getId() ?>
           id="newPlace"  method="post" novalidate
           enctype="multipart/form-data">

        <!-- NOMBRE -->
      <div class="form-group">
        <label for="name">Name</label>
        <input type="text" class="form-control" name="name" id="name"
               value="<?= $restaurant->getName() ?? ''?>"
               placeholder="<?= _("Introduce un nombre")?>">
      </div>

        <!-- DESCRIPCION -->
      <div class="form-group">
        <label for="description">Description</label>
        <textarea class="form-control" id="description" name="description" rows="3"
                  placeholder="<?= _("descripci&oacute;n")?>">
            <?= htmlspecialchars($restaurant->getDescription()); ?>

        </textarea>
      </div>

        <!-- COCINA -->
      <div class="form-group">
        <label for="cuisine"><?=_("Cocina") ?></label>
        <input type="text" class="form-control" name="cuisine" id="cuisine"
               value="<?= $restaurant->getCuisine() ?? ''?>" placeholder="<?=_("Cocina") ?>">
      </div>

        <!-- DIAS ABIERTOS -->
      <p><?=_("Dias abiertos")?></p>
      <div class="form-group">
        <div class="custom-control custom-control-inline custom-checkbox">
          <input type="checkbox" class="custom-control-input"
              <?=$restaurant->checkDayOpen("Monday")?'checked':'' ?>
                 id="checkMonday" name="days[]" value="1" >
          <label class="custom-control-label" for="checkMonday"><?=_("Lunes")?></label>
        </div>
        <div class="custom-control custom-control-inline custom-checkbox">
          <input type="checkbox" class="custom-control-input"
              <?=$restaurant->checkDayOpen("Tuesday")?'checked':'' ?>
                 id="checkTuesday" name="days[]" value="2" >
          <label class="custom-control-label" for="checkTuesday"><?=_("Martes")?></label>
        </div>
        <div class="custom-control custom-control-inline custom-checkbox">
          <input type="checkbox" class="custom-control-input"
              <?=$restaurant->checkDayOpen("Wednesday")?'checked':'' ?>
                 id="checkWednesday" name="days[]" value="3" >
          <label class="custom-control-label" for="checkWednesday"><?=_("Miercoles")?></label>
        </div>
        <div class="custom-control custom-control-inline custom-checkbox">
          <input type="checkbox" class="custom-control-input"
                 <?=$restaurant->checkDayOpen("Thursday")?'checked':'' ?>
                 id="checkThursday" name="days[]" value="4" >
          <label class="custom-control-label" for="checkThursday"><?=_("Jueves")?></label>
        </div>
        <div class="custom-control custom-control-inline custom-checkbox">
          <input type="checkbox" class="custom-control-input"
                 <?=$restaurant->checkDayOpen("Friday")?'checked':'' ?>
                 id="checkFriday" name="days[]" value="5" >
          <label class="custom-control-label" for="checkFriday"><?=_("Viernes")?></label>
        </div>
        <div class="custom-control custom-control-inline custom-checkbox">
          <input type="checkbox" class="custom-control-input"
                 <?=$restaurant->checkDayOpen("Saturday")?'checked':'' ?>
                 id="checkSaturday" name="days[]" value="6" >
          <label class="custom-control-label" for="checkSaturday"><?=_("Sabado")?></label>
        </div>
        <div class="custom-control custom-control-inline custom-checkbox">
          <input type="checkbox" class="custom-control-input"
                 <?=$restaurant->checkDayOpen("Sunday")?'checked':'' ?>
                 id="checkSunday" name="days[]" value="0" >
          <label class="custom-control-label" for="checkSunday"><?=_("Domingo")?></label>
        </div>
      </div>

        <!-- TELEFONO -->
      <div class="form-group">
        <label for="phone">Phone number</label>
        <input type="text" class="form-control" id="phone" name="phone" pattern="(\+?[0-9]2 ?)?[0-9]{9}"
               value="<?= $restaurant->getTelephone()?? ''?>" placeholder="<?=_("Tel&eacute;fono")?>">

      </div>

      <!-- CATEGORIA -->
      <div class="form-group">
          <label for="categoria"><?=_("Categor&iacute;a") ?></label>
          <select name="categoria" id="categoria">
              <?php foreach ($categorias ?? [] as $categoria): ?>
                    <option value=" <?=$categoria->getId() ?>"
                    <?=($restaurant->getCategoria() === $categoria->getId()) ? 'selected':''?>
                    ><?= $categoria->getNombre()?></option>
              <?endforeach; ?>
          </select>
      </div>


       <!-- IMAGEN -->
      <div class="form-group">
        <label for="image">Main photo</label>
        <input type="file" class="form-control" id="image" name="image"
        >
      </div>
        <?if($update==true): ?>
          <div>
              <input type="text" class="form-control d-none" name="lastImagen" id="lastImagen"
                     value="<?= $update ?$restaurant->getImagen():'' ?>"
                     placeholder="<?=_("Introduce un nombre") ?>">
          </div>
        <?endif;?>


      <img src="<?= $update ?'/'. $restaurant->getUrlPortafolio():'' ?>" alt="" id="imgPreview" class="img-thumbnail">

      <button type="submit" class="btn btn-primary"><?=_("A&ntilde;adir nuevo restaurante") ?></button>
    </form>



  </div>

    <script src="/js/addRestaurant.js"></script>




