


  <?php include __DIR__. '/partials/show-error.part.php' ?>

  <div class="container">
    <form action="/check-login" id="form-login" class="mt-4" method="POST" role="form">
      <legend><?= _("Bienvenido a FoodScore")?></legend>

      <div class="form-group">
        <label for="correo"><?= _("Correo electronico")?>:</label>
        <input type="email" class="form-control" name="email" id="email"
               value="<?= $email ?? '' ?>"
               placeholder="Email">
        <label for="password"><?= _("Contrase&ntilde;a")?>":</label>
        <input type="password" class="form-control" name="password" id="password"
               value="<?= $password ?? '' ?>"
               placeholder="<?= _("Contrase&ntilde;a")?>">
      </div>
      <p class="text-danger" id="errorInfo"></p>
      <a class="btn btn-secondary" href="register" role="button"><?= _("Crear cuenta")?></a>
      <button type="submit" class="btn btn-primary"><?= _("Iniciar sesi&oacute;n")?></button>
    </form>
  </div>



