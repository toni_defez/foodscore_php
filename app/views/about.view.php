<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 10/12/18
 * Time: 21:17
 */

?>

<div class="container">
    <div class="row">

        <div class="row">
            <div class="col-md-4 text-center">
                <div class="box">
                    <div class="box-content">
                        <h1 class="tag-title"><?=_("Plataforma de restaurantes")?></h1>
                        <hr />
                        <p>
                            <?=
                            _("Hemos desarrollado una soluci&oacute;n tecnol&oacute;gica que simplifica la forma en que se organizan y gestionan los restaurantes.")
                            ?>
                        </p>
                        <br />

                    </div>
                </div>
            </div>

            <div class="col-md-4 text-center">
                <div class="box">
                    <div class="box-content">
                        <h1 class="tag-title"><?= _("Solamente los mejores restaurantes") ?></h1>
                        <hr />
                        <p>
                            <?= _("
                            Adem&aacute;s, hemos construido una web de descubrimiento en la que siempre se encuentran restaurantes relevantes, 
                
                            ")?>
                        <br />

                    </div>
                </div>
            </div>
            <div class="col-md-4 text-center">
                <div class="box">
                    <div class="box-content">
                        <h1 class="tag-title"><?= _("Somos nosotros") ?></h1>
                        <hr />
                        <p>
                            <?= _("
                            FoodScore tambi&eacute;n somos  10 personas que trabajamos por todo el mundo para hacer que todo funcione.
                            ")?>
                            <br />

                    </div>
                </div>
            </div>






        </div>
    </div>
</div>



