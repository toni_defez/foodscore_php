<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 19/12/18
 * Time: 18:10
 */

?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">


    <div class="collapse navbar-collapse display:inline" id="navbarSupportedContent">

        <div class="container-fluid">
        <form class="form-inline my-2 my-lg-0"
              method="get" novalidate
              action="home?page=1">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">
                        <?= _("Categor&iacute;as")?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <select   class="form-control" name="categoria" id="categoria"  size="10">
                        <option  value="" >  <?= _(" Mostrar todas")?></a></option>
                        <?php foreach ($categorias ?? [] as $categoria): ?>
                            <option value=" <?=$categoria->getId() ?>"
                                <?=($categorySearch== $categoria->getId()) ? 'selected':''?>
                            ><?= $categoria->getNombre()?></option>
                        <?endforeach; ?>
                    </select>
                    </div>
                </li>

                <li class="nav-item">
                    <a class="nav-link" >  <?= _("Estrellas")?></a></a>
                </li>

                <li class="nav-item active" >
                    <input class="form-control mr-sm-2" type="number" name="numberStar" value="<?=$starSearch?>" min="0" max="5">
                </li>

                <li class="nav-item active">
                        <div class="checkbox form-control mr-sm-2" >
                            <label><input type="checkbox" value="Yes" name="open" <?=$onlyOpen=='Yes'?'checked':'' ?>
                                >  <?= _("Abiertos")?></label>
                        </div>
                </li>

                <li class="nav-item active">
                    <input class="form-control " name="name" type="text" value="<?=$nameSearch?>"
                           placeholder=" <?= _("Buscar")?>"
                           aria-label="Search">
                </li>

            </ul>
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit"> <?= _("Buscar")?></button>
        </form>
        </div>
    </div>
</nav>