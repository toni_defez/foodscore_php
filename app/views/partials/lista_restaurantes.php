
<div id="placesContainer" class="card-columns mt-3">


     <?php foreach ( ($restaurants ?? []) as $restaurant) :?>

       <div class="card">
        <img class="card-img-top" src='/<?= $restaurant->getUrlMiniatura() ?>'>
        <div class="card-body">
            <a href="/restaurant-details/<?=$restaurant->getId() ?>"> <h5 class="card-title"><?= $restaurant->getName()?></h5></a>
          <p class="card-text"><?= $restaurant->getDescription()?></p>
          <div class="card-text">
            <small class="text-muted">
              <strong>Opens: </strong> <?=$restaurant->getOpenDays()?>
            </small>
              <? if ($restaurant->isOpen()) :?>
            <span class="badge badge-success"> <?= _("Open")?></span>
              <?else:?>
            <span class="badge badge-danger"> <?= _("Close")?></span>
              <? endif;?>
          </div>
          <div class="card-text">
            <small class="text-muted">
              <strong>Phone: </strong> <?= $restaurant->getTelephone() ?>
            </small>
          </div>
            <div class="card-text">
                <small class="text-muted">
                    <strong> <?= _("Categor&iacute;a")?>: </strong> <?= $queryBuilder->getCategoria($restaurant)->getNombre() ?>
                </small>
            </div>
            <div class="card-text">
                <small class="text-muted">
                    <strong><?= _("Due&ntildeo")?>: </strong>
                    <a href="/profile/<?= $restaurant->getuserId()?>">
                        <?= $queryBuilderUser->find($restaurant->getuserId())->getName()?>
                    </a>
                </small>
            </div>
            <? if ($restaurant->isMine()  || ( $app['user']!="" && $app['user']->getRol() ?? "" === 'ROLE_ADMIN')):?>
                <button class="mt-2 btn btn-sm btn-danger btn-delete" id="<?= $restaurant->getId() ?>">
                    <?= _("Borrar")?></button>
                <a href="/edit-restaurant/<?=$restaurant->getId()?>">
                    <button class="mt-2 btn btn-sm btn-info btn-info"><?= _("Editar")?></button>
                </a>

            <?endif;?>
        </div>
           <div class="card-text mx-auto text-center">
               <small class="text-muted">
               <?for ($i = 0; $i < $restaurant->getNumberStar(); $i++)
               {
                   echo "<i class=\"fas fa-star fa-2x\"></i>";
               }?>
               </small>
           </div>
        <div class="card-footer">
          <small class="text-muted">
            <?= $restaurant->getCuisine()?>
          </small>
            <small class="text-muted">

            </small>
        </div>
      </div>

    <?endforeach;?>
    </div>
  </div>