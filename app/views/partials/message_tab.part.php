



<div class=" col-sm-2 card card-body bg-light ">
    <table class="table table-inbox table-hover">
        <tbody>
        <tr class="">
            <td class="view-message">
                <a href="/message_send/<?=$id?>"> <?= _("Ver mensajes enviados")?></a>

            </td>
        </tr>
        <tr class="">
            <td class="view-message">
                <a href="/message/<?=$id?>"> <?= _("Ver mensajes recibidos")?></a>
            </td>
        </tr>

        <? if(( $app['user']!="" && $app['user']->getId()===$id)):?>
        <tr class="">
            <td class="view-message">
                <a href="/show-sendMessage/<?=$id?>"> <?= _("Crear nuevo mensaje")?></a>
            </td>
        </tr>
        <?endif;?>


        </tbody>
    </table>
</div>