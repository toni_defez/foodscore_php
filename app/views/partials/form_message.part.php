

<?php if (!empty($mensaje) || !empty($errores)) :?>

    <div class="alert alert-<?=empty($errores) ?'info':'danger';?> alert-dismissible" role = "alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">x</span>
        </button>
        <?php if (empty($errores)):?>
            <p><?= $mensaje ?></p>
        <?php else : ?>
            <ul>
                <?php foreach($errores as $error) : ?>
                    <li><?= $error ?></li>
                <?php endforeach;?>
            </ul>
        <?php endif;?>
    </div>

<?endif; ?>
<h3>Ponerse en contacto</h3>
<form action="/new-message/<?=$usuario->getId()?>" method="post">
    <div class="form-group">
        <label for="title"><?= _("Titulo")?></label>
        <input type="text" class="form-control" id="title" value="<?= $title??''?>" name="title">
    </div>
    <div class="form-group">
        <label for="text"><?= _("Mensaje")?></label>
        <textarea class="form-control" id="text" name="text" rows="3">
            <?= $text?? ""?>
        </textarea>
    </div>
    <button type="submit" class="btn btn-primary"><?= _("Enviar mensaje")?></button>
</form>