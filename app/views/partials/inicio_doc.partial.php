
<?php
use dwes\app\utils\Utils;
?>


<html lang="<?php echo (isset($_SESSION['idioma']) ? substr($_SESSION['idioma'], 0, 2) : 'es'); ?>">
<head>
  <title>FoodScore</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
  crossorigin="anonymous">
    <link rel="stylesheet" href="../styles.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css"
   integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  <style>
#map {
width: 100%;
      height: 300px;
    }
  </style>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>

<body>
  <nav class="navbar navbar-expand navbar-dark bg-dark">
    <a class="navbar-brand" href="">FoodScore</a>
    <ul class="navbar-nav mr-auto">

      <li class="nav-item">
        <a href="/home" class="nav-link <?= Utils::isActiveMenu('/home') ? 'active': '' ?>">
            <?=_("Inicio")?></a>
      </li>
        <?php if (!is_null($app['user'])) :?>
      <li class="nav-item">
          <a href="/add-restaurant" class="nav-link <?= Utils::isActiveMenu('/add-restaurant') ? 'active': '' ?>">
              <?= _("A&ntilde;adir restaurante")?></a>
      </li>
      <li class="nav-item">
          <a href="/profile/<?= $app['user']->getId()?>" class="nav-link <?= Utils::isActiveMenu('/profile') ? 'active': '' ?>">
              <?= _("Mi perfil")?></a>
      </li>
      <li class="nav-item">
            <a href="/message/<?= $app['user']->getId() ?>" class="nav-link <?= Utils::isActiveMenu('/message') ? 'active': '' ?>">
                <?= _("Mis mensajes ")?>
            </a>
      </li>

        <?php if($app['user']->getRol() === 'ROLE_ADMIN'):?>
                <a href="/list_user" class="nav-link <?= Utils::isActiveMenu('/list_user') ? 'active': '' ?>">
                    <?= _("Lista de usuarios")?></a>
        <?endif;?>


        <?endif;?>
        <li class="nav-item">
            <a href="/about" class="nav-link <?= Utils::isActiveMenu('/about') ? 'active': '' ?>">
                <?= _("Sobre nosotros")?></a>
        </li>


    </ul>
    <ul class="navbar-nav">

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown09" data-toggle="dropdown"
               aria-haspopup="true" aria-expanded="false">
                <span class="flag-icon flag-icon-us"> </span>

                <? if($_SESSION['idioma'] == "es_ES" || $_SESSION['idioma'] == "es") {
                        echo _("Espa&ntilde;ol");
                    }
                    else
                        echo _("Ingl&eacute;s");
                ?>

            </a>
            <div class="dropdown-menu" aria-labelledby="dropdown09">
                <a class="dropdown-item"  href="/idioma/1"><span class="flag-icon flag-icon-fr"> </span>
                    <?= _("Espa&ntilde;ol")?></a>
                <a class="dropdown-item" href="/idioma/2"><span class="flag-icon flag-icon-it"> </span>
                    <?= _("Ingl&eacute;s")?></a>

            </div>
        </li>

        <?php if (is_null($app['user'])) :?>
            <li class="nav-item">
                <a class="nav-link" href="/login" id="login">   <?= _("Inciar sesi&oacute;n")?></a>
            </li>

        <?else:?>
            <li class="nav-item">
                <a class="nav-link" href="/logout" id="logout">    <?= _("Cerrar sesi&oacute;n")?></a>
            </li>
        <?endif;?>


    </ul>
  </nav>
