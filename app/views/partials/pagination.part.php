<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 15/12/18
 * Time: 17:54
 */












?>
<?php
$pageNext = $pageSelected +1 ;
if($pageNext> $number_of_pages){
    $pageNext = $number_of_pages;}

$pagePrev = $pageSelected-1;
if( $pagePrev <=0)
    $pagePrev = 1;
?>

<nav aria-label="...">
    <ul  class="pagination justify-content-center">
        <li class="page-item ">
           <a  tabindex="-1" class="page-link" href= "<?=$url .$pagePrev?>">
               <?=_("Previo")?>
               </a>
        </li>
        <?php for($page =1;$page<=$number_of_pages ;$page ++):?>
        <li class="page-item  <?=$page== $pageSelected?'active':'';?>" >
            <?= '<a   class="page-link" href="'.$url .$page.'">'. $page.'</a>';?>
        </li>
        <?php endfor; ?>

        <li class="page-item ">
            <a  tabindex="-1" class="page-link" href="<?=$url .$pageNext?>">
                <?=_("Siguiente")?>
            </a>
        </li>
    </ul>
</nav>









