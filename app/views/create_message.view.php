
<div class="container card bg-light top-buffer">

    <div class="row .mt-20">
        <h1>Mensajes de <?= $nameUser?></h1>
    </div>
    <div class="row .mt-20">
    <?php include __DIR__ . '/partials/message_tab.part.php'; ?>
        <div class="col-sm-10">

            <div class="card">
                <div class="card-body">
                    <?php if (!empty($mensaje) || !empty($errores)) :?>
                        <div class="alert alert-<?=empty($errores) ?'info':'danger';?> alert-dismissible" role = "alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">x</span>
                            </button>
                            <?php if (empty($errores)):?>
                                <p><?= $mensaje ?></p>
                            <?php else : ?>
                                <ul>
                                    <?php foreach($errores as $error) : ?>
                                        <li><?= $error ?></li>
                                    <?php endforeach;?>
                                </ul>
                            <?php endif;?>
                        </div>
                    <?endif; ?>
                    <hr>
                    <h4 ><?=_("Crear nuevo mensaje") ?></h4>
                    <form action="/new-message" method="post">
                        <div class="form-group">
                            <label for="title"><?= _("Destinatario")?></label>
                            <input type="text" class="form-control" id="destinatario"
                                   value="<?=$destinatario??"" ?>" name="destinatario">
                        </div>
                        <div class="form-group">
                            <label for="title"><?= _("Titulo")?></label>
                            <input type="text" class="form-control" id="title"
                                   value="<?=$titulo ?? "" ?>" name="title">
                        </div>
                        <div class="form-group">
                            <label for="text"><?= _("Mensaje")?></label>
                            <textarea class="form-control" id="text" name="text" rows="6">
                            </textarea>
                        </div>
                        <button type="submit" class="btn btn-primary"><?=_("Enviar mensaje") ?></button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>