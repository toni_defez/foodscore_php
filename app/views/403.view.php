<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 14/12/18
 * Time: 0:23
 */
?>
<div class="page-wrap d-flex flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <span class="display-1 d-block">304</span>
                <div class="mb-4 lead"><?=_("La p&aacute;gina que buscas no ha sido encontrada")?></div>
                <a href="/" class="btn btn-link"><?=_("Volver al inicio") ?></a>
            </div>
        </div>
    </div>
</div>
