


<?php if (!empty($mensaje) || !empty($errores)) :?>

    <?php include __DIR__ . '/partials/show-error.part.php'; ?>

<?endif; ?>


  <div class="container">
    <form action=" /check-registro" id="form-register" class="mt-4" method="POST" role="form"
          novalidate
          enctype="multipart/form-data">
      <legend><?= _("Crear una cuenta")?></legend>

      <div class="form-group">
        <label for="nombre"><?=_("Nombre") ?>:</label>
        <input type="text" class="form-control" id="name" name="nameUser"
               value= "<?= $nombre ?>" placeholder="<?= _("Nombre")?>">
      </div>
      <div class="form-group">
        <label for="correo"><?=_("Correo electronico")?>:</label>
        <input type="email" class="form-control" id="email" name="email" placeholder="Email"
               value= "<?= $email ?>" >
      </div>
      <div class="form-group">
        <label for="correo"><?= _("Repita el correo electronico")?>:</label>
        <input type="email" class="form-control" id="email2" name="email2" placeholder="Email"
               value= "<?= $email2 ?>" >
      </div>

        <div class="form-group">
            <label for="birthday"><?= _("Fecha de nacimiento") ?>:</label>
            <input type="date" required class="form-control" id="birthday" name="birthday" placeholder="dd/mm/yyyy"
                   value= "<?= $birthday ?>" >
        </div>
      <div class="form-group">
        <label for="password"><?= _("Contrase&ntilde;a")?>:</label>
        <input type="password" class="form-control"  required id="password" name="password" placeholder="Password"
         >
      </div>
        <div class="form-group">
            <label for="captcha_code"><?= _("C&oacute;digo Captcha ")?>:</label>
            <img src="web/captcha.php">
            <input type="text" name="captcha_code"   class="form-control"  placeholder="Captcha">
        </div>
        <div class="form-group">
            <label for="languaje"><?= _("Idioma") ?>:</label>
            <select   class="form-control" name="languaje" id="languaje" >
                <option  value="es_ES"   <?=("es_ES"=== $languaje) ? 'selected':''?> ><?=_("Espa&ntilde;ol") ?></option>
                <option  value="en_GB"   <?=("en_GB"=== $languaje) ? 'selected':''?> ><?= _("Ingl&eacute;s")?></option>
            </select>
        </div>


      <div class="form-group">
        <label for="image"><?= _("Image de perfil") ?></label>
        <input type="file" class="form-control" id="image" name="image">
      </div>
      <img src="" alt="" id="imgPreview" class="img-thumbnail">
      <div class="row">


      </div>
      <a class="btn btn-secondary" href="login.php" role="button"><?= _("Volver atr&aacute;s")?></a>
      <button type="submit" class="btn btn-primary"><?= _("Crear cuenta")?></button>
    </form></div>
