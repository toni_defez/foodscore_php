<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 10/12/18
 * Time: 1:00
 */

namespace dwes\app\utils;


use dwes\core\App;
use Swift_Mailer;
use Swift_Message;
use Swift_SmtpTransport;

class MyMailer
{
    private $mailer;
    private $config;

    private function __construct()
    {
        $this->config = App::get('config')['mailer'];
        $config = $this->config;
        $transport = (new Swift_SmtpTransport($config['host'], $config['port'] ))
            ->setUsername($config['username'])
            ->setPassword($config['password'])->setEncryption($config['encryption']);

        $this->mailer = new Swift_Mailer($transport);
    }

    public static function load():MyMailer{
        return new MyMailer();
    }

    public function sendMessage(string $message1,$email):int{
        $this->config = App::get('config')['mailer'];
        $config = $this->config;

        $message = (new Swift_Message('Nuevo Restaurante añadido!!'))
            ->setFrom([$config['username'] => $config['alias']])
            ->setTo([$email=> 'Brandom'])
            ->setBody($message1);
        // Send the message
        return $this->mailer->send($message);
    }

}