<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 19/11/18
 * Time: 18:10
 */
namespace dwes\app\utils;
use dwes\core\App;
use dwes\app\exceptions\AppException;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MyLog
{
    /**
     * @var Logger
     */
    private $log;

    /**
     * MyLog constructor.
     * @param string $filaname
     */
    private function __construct()
    {
        try {
            $config = App::get('config')['logger'];
            $this->log = new Logger($config['name']);
            $this->log->pushHandler(
                new StreamHandler($config['file'], Logger::INFO)
            );
        }
        catch (AppException $e) {
        }
        catch (\Exception $e) {
        }
    }

    /**
     * @return MyLog
     */
    public static function load():MyLog{
        return new MyLog();
    }

    /**
     * @param string $message
     */
    public function add(string $message):void{
        $this->log->addInfo($message);
    }


}