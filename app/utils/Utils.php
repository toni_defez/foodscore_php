<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 10/12/18
 * Time: 19:42
 */

namespace dwes\app\utils;


class Utils
{
    public static function isActiveMenu(string $menuOption) : bool
    {
            return (strpos($_SERVER['REQUEST_URI'], $menuOption) !== false);
    }
}