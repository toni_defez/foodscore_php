<?php
namespace  dwes\app\utils;

use dwes\app\exceptions\FileException;
use Exception;

define('THUMBNAIL_IMAGE_MAX_WIDTH', 660);
define('THUMBNAIL_IMAGE_MAX_HEIGHT', 660);

class File{

    private $file;
    private $fileName;

    /**
     * File constructor.
     * @param string $fileName
     * @param array $arrTypes
     * @throws FileException
     */
    public function __construct( string $fileName, array $arrTypes)
    {
        //en $_FILES obtenemos los archivos que se han subido
        $this->file = $_FILES[$fileName];
        $this->fileName='';

        if(!isset($this->file)){
           throw  new FileException("Debes seleccionar un fichero");
        }

        //nos dice si se ha producido algun error al subid el fichero
        if($this->file['error'] !== UPLOAD_ERR_OK){
            //vamos a verificar que fallo hemos tenido

            switch ($this->file['error']){
                case UPLOAD_ERR_INI_SIZE:
                case UPLOAD_ERR_FORM_SIZE:
                    //fallo en el tamaño
                    throw  new FileException("Fichero demasiado grande");
                case UPLOAD_ERR_PARTIAL:
                    //FICHERO INCOMPLETO
                    throw  new FileException("No se ha podido subir el fichero completo");
                default:
                //error generico
                    throw  new FileException("No se ha podido subir el fichero");
                    break;
            }
        }

        //comprobando el formato del fichero
        if( in_array($this->file['type'],$arrTypes)==false)
            throw  new FileException("Formato no correcto");

        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }


    /**
     * @param string $rutaDestino
     * @throws FileException
     *
     */
    public  function saveUploadFile(string $rutaDestino){
        //comprobamos que el fichero que se ha subido ha sido
        // a traves de un formulario usando post
        if(is_uploaded_file($this->file['tmp_name']) === false){
            throw  new FileException('El archivo no ha sido subido mediante formulario');
        }

        $this->fileName = $this->file['name'];
        $ruta = $rutaDestino. $this->fileName;

        //devuelve true si el archivo ya existe
        if(is_file($ruta)==true){
            //creamos un nuevo nombre
            $idUnico = time();//valor milisegundo para nombre aleatorio
            $this->fileName = $idUnico . '_' . $this->fileName;
            $ruta = $rutaDestino . $this->fileName;
        }


        //movemos el archivo de la carpeta temporal a la carpeta del proyecto

        if(move_uploaded_file($this->file['tmp_name'],$ruta)=== false)
            throw  new FileException("No se puede mover el fichero a su destino");

        // Obtenemos la ruta donde pondremos el archivo y le añadimos 'min_' para diferenciarlas de las imagenes normales
          $thumbnail_image_path = $rutaDestino.'min_'.$this->fileName;
        // Creamos la thumbnail
        $this->generate_image_thumbnail($ruta, $thumbnail_image_path);



    }


    /**
     * @param $source_image_path
     * @param $thumbnail_image_path
     * @return bool
     * @throws Exception
     */
    function generate_image_thumbnail($source_image_path, $thumbnail_image_path)
    {


        if(!file_exists($source_image_path))
            throw new Exception('La imagen '.$source_image_path.' no existe');

        $info = getimagesize($source_image_path);

        if($info === false)
            throw new Exception('No es un archivo válido');

        list($source_image_width, $source_image_height, $source_image_type) = $info;
        $source_gd_image = null;

        switch($source_image_type)
        {
            case 2:
                $source_gd_image = imagecreatefromjpeg ( $source_image_path );
                break;
            case 3:
                $source_gd_image = imagecreatefrompng ( $source_image_path );
                break;
            default:
                throw new Exception('Esto no es un archivo JPG o PNG');
        }

        if ($source_gd_image === false)
        {
            return false;
        }

        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = THUMBNAIL_IMAGE_MAX_WIDTH / THUMBNAIL_IMAGE_MAX_HEIGHT;

        if ($source_image_width <= THUMBNAIL_IMAGE_MAX_WIDTH && $source_image_height <= THUMBNAIL_IMAGE_MAX_HEIGHT)
        {
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        }
        else if ($thumbnail_aspect_ratio > $source_aspect_ratio)
        {
            $thumbnail_image_width = (int) (THUMBNAIL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
            $thumbnail_image_height = THUMBNAIL_IMAGE_MAX_HEIGHT;
        }
        else
        {
            $thumbnail_image_width = THUMBNAIL_IMAGE_MAX_WIDTH;
            $thumbnail_image_height = (int) (THUMBNAIL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
        }

        $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);

        $col_transparent = imagecolorallocatealpha($thumbnail_gd_image, 255, 255, 255, 127);
        imagefill($thumbnail_gd_image, 0, 0, $col_transparent);  // set the transparent colour as the background.
        imagecolortransparent ($thumbnail_gd_image, $col_transparent); // actually make it transparent

        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);

        switch($source_image_type)
        {
            case 2:
                //header( "Content-type: image/jpeg" );
                imagejpeg($thumbnail_gd_image, $thumbnail_image_path);
                break;
            case 3:
                //header( "Content-type: image/png" );
                imagepng($thumbnail_gd_image, $thumbnail_image_path, 9);
                break;
            default:
                throw new Exception('Esto no es un archivo JPG o PNG');
        }

        imagedestroy($source_gd_image);
        imagedestroy($thumbnail_gd_image);
        return true;

    }


    /**
     * @param string $rutaOrigen
     * @param string $rutaDestino
     * @throws FileException
     */
    public function copyFile(string $rutaOrigen,string $rutaDestino){
        $origen = $rutaOrigen . $this->fileName;
        $destino = $rutaDestino . $this->fileName;

        if(is_file($origen)==false) //no existe el fichero
            throw new FileException("No existe el fichero $origen que quieres copiar");

        if( is_file($destino) === true)
            throw new FileException("El fichero $destino ya existe y no se puede sobrescribir");

        if(copy($origen,$destino)==false){
            throw new FileException("No se ha podido copiar el fichero $origen a $destino");
        }
    }

}

?>