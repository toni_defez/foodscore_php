<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 31/12/18
 * Time: 22:02
 */

namespace dwes\app\controllers;

use dwes\app\repository\UserRepository;
use dwes\core\App;
use dwes\core\helpers\FlashMessage;

class InternacionalizationController
{
    public  static function detectarIdioma(){
        $idioma =substr($_SERVER["HTTP_ACCEPT_LANGUAGE"],0,2);
        return $idioma;
    }


    public function cambiaIdioma(string $idioma)
    {
        switch($idioma)
        {
            case 1:
                $_SESSION['idioma'] = 'es_ES';
                break;
            case 2:
                $_SESSION['idioma'] = 'en_GB';
                break;

            default:
                $_SESSION['idioma'] = 'es_ES';
                break;
        }
        App::get('router')->redirect('');
    }

    public static function cambiaIdioma2(string $idioma)
    {
        $_SESSION['idioma'] = $idioma;
    }
}