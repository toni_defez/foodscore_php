<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 10/12/18
 * Time: 23:40
 */
namespace  dwes\app\controllers;
use DateTime;
use dwes\app\entity\Usuario;
use dwes\app\exceptions\FileException;
use dwes\app\repository\CommentsRepository;
use dwes\app\repository\RestaurantRepository;
use dwes\app\repository\UserRepository;
use dwes\app\utils\File;
use dwes\core\App;
use dwes\core\helpers\FlashMessage;
use dwes\core\Response;
use dwes\core\Security;

class UserController
{

    public function login(){
        Response::renderView('login','layout');
    }

    public function register(){

        $errores = [];

        $reviewFields=function ($nombre,$email,$email2,$password,$birthday) use (&$errores,&$usuarios){

            if(!empty($nombre) && !empty($email) && !empty($email2) && !empty($password)
                && !empty($birthday))
            {

                if( filter_var($email,FILTER_VALIDATE_EMAIL)=== false or
                    filter_var($email2,FILTER_VALIDATE_EMAIL)===false){
                    $errores[] = "Los dos email deben de ser validos";
                }
                else if ( $email !== $email2){
                    $errores[] = "Los dos email deben ser iguales";
                }
                else if(DateTime::createFromFormat('Y-m-d',$birthday)===false){
                    $errores[] = "La fecha no es correcta";
                }
                else if( $_POST['captcha_code'] != $_SESSION["captcha_code"] ){
                    $errores[] = "Captcha no valido";
                }
                else {
                    echo "<p>Todo correcto para insertar en base de datos </p>";
                    $usuarios[]=['nombre'=>$nombre,'email'=>$email];
                }
            }
            else
            {
                echo "<p> alguno de los campos se ha quedado vacio </p>";
                $errores[]="Alguno de los campos es vacio";
            }
        };

        if($_SERVER['REQUEST_METHOD']==='POST'){
            $nombre = trim( htmlspecialchars($_POST['nameUser']));
            $email = trim( htmlspecialchars($_POST['email']));
            $email2 = trim(htmlspecialchars($_POST['email2']));
            $password = trim(htmlspecialchars($_POST['password']));
            $birthday = trim(htmlspecialchars($_POST['birthday']));
            $languaje = trim(htmlspecialchars($_POST['languaje']));
            $reviewFields($nombre,$email,$email2,$password,$birthday);
        }
        else
        {
            $nombre = "";
            $email = "";
            $email2 = "";
            $password = "";
            $birthday = "";
        }

        Response::renderView('register','layout',compact('usuarios',
            'nombre','errores','email2','email','password','birthday','languaje'));
    }

    public function edit_profile($id){
        $usuarioCurrent = App::get('appUser') ?? '';

        if($usuarioCurrent =='')
        {
            App::get('router')->redirect('');
        }
        else if($usuarioCurrent->getRol() === 'ROLE_ADMIN' || $usuarioCurrent->getId() == $id){
            $usuario = App::getRepository(UserRepository::class)->find($id);
            $errores = FlashMessage::get('errores')?? [];
            $mensaje = FlashMessage::get('mensaje') ?? '';
            Response::renderView('edit-profile','layout',compact('usuario','errores','mensaje'));
        }
    }

    public function changeNameEmail($id){
        $usuario = App::getRepository(UserRepository::class)->find($id);
        $name =  trim( htmlspecialchars($_POST['nameUser']));
        $email =  trim( htmlspecialchars($_POST['email']));

        $usuario->setName($name);
        $usuario->setEmail($email);

        try{
            App::getRepository(UserRepository::class)->update($usuario);
        }
        catch (\Exception $e){
            FlashMessage::set('errores',[$e->getMessage()]);
        }
        FlashMessage::set('mensaje',"Se ha llevado a cabo el cambio en el perfil");
        App::get('router')->redirect('edit-profile/'.$usuario->getId());
    }

    public function changePassword($id){
        $usuario = App::getRepository(UserRepository::class)->find($id);
        $password1 =  trim( htmlspecialchars($_POST['password']));
        $password2 =  trim( htmlspecialchars($_POST['password2']));

        if($password1 !== $password2)
            FlashMessage::set('errores',['Las dos contraseñas deben ser iguales']);
        else{
            try {
                $usuario->setPassword(Security::encrypt($password1));
                App::getRepository(UserRepository::class)->update($usuario);
            }
            catch (\Exception $e){
                FlashMessage::set('errores',[$e->getMessage()]);
            }
            FlashMessage::set('mensaje',"Se ha llevado a cabo el cambio en el perfil");
            App::get('router')->redirect('edit-profile/'.$usuario->getId());
        }
    }

    public function changeAvatar($id){
        $tiposAceptados = ['image/jpeg', 'image/png','image/gif'];

        try {
            $usuario = App::getRepository(UserRepository::class)->find($id);
            $imagen = new File('image',$tiposAceptados);
            $imagen->saveUploadFile(Usuario::RUTA_IMAGENES_USUARIOS);
            $usuario->setAvatar($imagen->getFileName());
            App::getRepository(UserRepository::class)->update($usuario);

        } catch (FileException $e) {
            FlashMessage::set('errores',[$e->getMessage()]);
        }
        catch (\Exception $e){
            FlashMessage::set('errores',[$e->getMessage()]);
        }
        FlashMessage::set('mensaje',"Se ha llevado a cabo el cambio en el perfil");
        App::get('router')->redirect('edit-profile/'.$usuario->getId());
    }

    public function changeLanguaje($id){
        $languaje = $_POST['languaje'];
        try{
            $usuario = App::getRepository(UserRepository::class)->find($id);
            $usuario->setIdioma($languaje);
            App::getRepository(UserRepository::class)->update($usuario);
            InternacionalizationController::cambiaIdioma2($usuario->getIdioma());
        }
        catch (\Exception $e){
            FlashMessage::set('errores',[$e->getMessage()]);
        }
        FlashMessage::set('mensaje',"Se ha llevado a cabo el cambio en el perfil");
        App::get('router')->redirect('edit-profile/'.$usuario->getId());

    }

    public function show_profile($id){

        /*Control of message*/
       $errores = FlashMessage::get('errores');
       $mensaje = FlashMessage::get('message');

       $title = FlashMessage::get('title') ;
       $text = FlashMessage::get('text');


        $soyYo=true;
        $pageSelected = $_GET['page']?? 1 ;
        $usuario = "";
        if($id === App::get('appUser')->getId()){
            //soy yo
            $usuario = App::get('appUser');
        }
        else {
            // no soy yo
            $soyYo = false;
            $usuario = App::getRepository(UserRepository::class)->find($id);
        }
        $queryBuilder = App::getRepository(RestaurantRepository::class);
        $totalRestaurants = $queryBuilder->count(["userId"=>$id]);
        $result_per_page = 5;
        $number_of_pages = ceil($totalRestaurants/$result_per_page);
        $this_page_first_result = ($pageSelected-1)*$result_per_page;
        $restaurants = $queryBuilder->getElementLimit($this_page_first_result, $result_per_page,["userId"=>$id]);
        $queryBuilderUser = App::getRepository(UserRepository::class);
        $url ="/profile/".$id."?page=";
        $queryBuilder->getElementLimit($this_page_first_result, $result_per_page,["active"=>true]);
        $comments = App::getRepository(CommentsRepository::class)->getAllCommentsByUsuario($id);
        Response::renderView('profile','layout',compact('usuario','soyYo','restaurants',
            'queryBuilder','comments','number_of_pages','queryBuilderUser' ,'pageSelected','url','errores','mensaje','title','text'));
    }

    public function listAlluser(){
        $parameters = [];
        $parameters['active'] = true;
        $parameters['rol'] = 'ROLE_USER';
        $pageSelected = $_GET['page'] ?? 1;
        $totalRestaurants = App::getRepository(UserRepository::class)->count($parameters);
        $result_per_page =  5;
        $number_of_pages = ceil($totalRestaurants/$result_per_page);
        $url ="/list_user?";

        $this_page_first_result = ($pageSelected-1)*$result_per_page;
        foreach ($parameters as $key=>$value){
            $url.=$key."=".$value."&";
        }
        $url.="page=";

        try {
            $usersList = App::getRepository(UserRepository::class)->getElementLimit($this_page_first_result, $result_per_page,$parameters);
        }
        catch (\Exception $a){
            echo 'Error '.' '.$a->getMessage();
        }
        Response::renderView('list_user','layout',compact('usersList','number_of_pages',
            'pageSelected','url'));
    }




}