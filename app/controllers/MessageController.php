<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 1/01/19
 * Time: 18:25
 */

namespace dwes\app\controllers;


use dwes\app\entity\Message;
use dwes\app\exceptions\QueryException;
use dwes\app\exceptions\ValidationException;
use dwes\app\repository\MessageRepository;
use dwes\app\repository\UserRepository;
use dwes\core\App;
use dwes\core\helpers\FlashMessage;
use dwes\core\Response;

class MessageController
{
    public function newMessage($idDestinatario){
        $errores = [];
        $mensaje = "Mensaje enviado";
        try {
            $tmpmessage = $this->CreateMessage($idDestinatario);
            App::getRepository(MessageRepository::class)->save($tmpmessage);
        }
        catch (ValidationException $e){
            $errores[]=$e->getMessage();
        }
        catch (\Exception $e){
            $errores[]=$e->getMessage();
        }


        //TODO ENVIAR MENSAJE AL DESTINARIO POR CORREO PARA INDICARLE QUE HA RECIBIDO UN NUEVO MENSAJE
        FlashMessage::set('errores',$errores);
        FlashMessage::set('message',$mensaje);

        if(!empty($errores))
        {
            FlashMessage::set('title',$_POST['title']);
            FlashMessage::set('text',$_POST['text']);
        }

        App::get('router')->redirect('profile/'.$idDestinatario);
    }
    /**
     * @throws \dwes\app\exceptions\AppException
     */
    public function showMessage($id){
        $errores = FlashMessage::get('errores');
        $mensaje = FlashMessage::get('message');
        $userCurrent =App::get('appUser');
        if($userCurrent =='')
        {
            App::get('router')->redirect('');
        }
        else if($userCurrent->getRol() === 'ROLE_ADMIN' || $userCurrent->getId() == $id){
            $messages = App::getRepository(MessageRepository::class)->getAllMessageByUsuario($id);
            $nameUser = App::getRepository(UserRepository::class)->find($id)->getName();
            Response::renderView('show-message','layout',compact('messages','id','nameUser',
                'errores','mensaje'));
        }
        else
        {
            Response::renderView('403','layout');
        }

    }

    public function showDetailMessage(){
        $id = $_GET['id'];
        $messageId = $_GET['idmessage'];
        $user = App::get('appUser');
        try{
            if($user =='')
            {
                App::get('router')->redirect('');
            }
            else if($user->getRol() === 'ROLE_ADMIN' || $user->getId() == $id){
                $message = App::getRepository(MessageRepository::class)->getMessageById($messageId,$id,
                    $user->getRol() === 'ROLE_ADMIN' );
                if($message)
                    $soyYo = $user->getEmail() == $message['remitente'];
                $nameUser = App::getRepository(UserRepository::class)->find($id)->getName();
                Response::renderView('detail_message','layout',compact('message','soyYo','id','nameUser'));
            }
            else
            {
                Response::renderView('403','layout');
            }
        }
        catch (\Exception $e)
        {
            Response::renderView('403','layout');
        }

    }

    public function listMessageSendByMe($id){

        $userCurrent =App::get('appUser');
        if($userCurrent =='')
        {
            App::get('router')->redirect('');
        }
        else if($userCurrent->getRol() === 'ROLE_ADMIN' || $userCurrent->getId() == $id)
        {
            $nameUser = App::getRepository(UserRepository::class)->find($id)->getName();
            $messages = App::getRepository(MessageRepository::class)->getAllMessageSentByUsuario($id);
            Response::renderView('show-message','layout',compact('messages','id','nameUser'));
        }
        else
        {
            Response::renderView('403','layout');
        }
    }

    public function showSendMessage($id){
        $userCurrent =App::get('appUser');
        $destinatario = $_POST['destinatario']??"";
        $titulo = $_POST['titulo']??"";
        if($userCurrent =='')
        {
            App::get('router')->redirect('');
        }
        else if($userCurrent->getRol() === 'ROLE_ADMIN' || $userCurrent->getId() == $id)
        {
            $nameUser = App::getRepository(UserRepository::class)->find($id)->getName();

            Response::renderView('create_message','layout',compact('messages','destinatario',
                'titulo','id','nameUser'));
        }
        else
        {
            Response::renderView('403','layout');
        }


    }

    public function deleteMessage(){
        $id = $_GET['id'];
        $messageId = $_GET['idmessage'];
        $userCurrent =App::get('appUser');
        $message = App::getRepository(MessageRepository::class)->find($messageId);
        $errores = [];
        $message ="Borrado correcto";
        if($userCurrent =='' || $message == '')
        {
            App::get('router')->redirect('');
        }
        else if($userCurrent->getRol() === 'ROLE_ADMIN' ||
            ( $id== $message->getRemitenteId()
            || $id== $message->getDestinatarioId())
        )
        {
            try {
                App::getRepository(MessageRepository::class)->delete($messageId);

            }
            catch (\Exception $e){
                    $errores[]=$e->getMessage();
            }
            FlashMessage::set('errores',$errores);
            FlashMessage::set('message',$message);
            App::get('router')->redirect('message/' . $id);
        }
        else
        {
            Response::renderView('403','layout');
        }
    }

    public function newMessageWithEmail(){
        $errores = [];
        $mensaje = "Mensaje enviado";
        $emailDestinatario = $_POST['destinatario'];
        try
        {
            $destinatario = App::getRepository(UserRepository::class)->findBy(["email"=>$emailDestinatario]);
            if(empty($destinatario)){
                throw  new QueryException("No existe ese usuario");
            }
            $tmpmessage = $this->CreateMessage($destinatario[0]->getId());
            App::getRepository(MessageRepository::class)->save($tmpmessage);
        }
        catch (\Exception $exception){
            $errores[]=$exception->getMessage();
        }
        FlashMessage::set('errores',$errores);
        FlashMessage::set('message',$mensaje);

        if(!empty($errores))
        {
            FlashMessage::set('title',$_POST['title']);
            FlashMessage::set('text',$_POST['text']);
            FlashMessage::set('destinatario',$_POST['destinatario']);
        }
        $id =App::get('appUser')->getId();
        $nameUser =App::get('appUser')->getEmail();
        Response::renderView('create_message','layout',compact('mensaje','errores','id','nameUser'));

    }

    /**
     * @param $idDestinatario
     * @return Message
     * @throws \dwes\app\exceptions\AppException
     */
    private function CreateMessage($idDestinatario): Message
    {
        $test_input = function ($data, $key) {
            if (empty($data))
                throw  new ValidationException("Please complete input " . $key);
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        };

        $idRemitente = App::get('appUser')->getId();
        $title = $test_input($_POST['title'], "title");
        $text = $test_input($_POST['text'], "text");
        $tmpmessage = new Message($idRemitente, $idDestinatario, $title, $text);
        return $tmpmessage;
    }


}