<?php

namespace  dwes\app\controllers;

use dwes\app\entity\Comment;
use dwes\app\exceptions\AppException;
use dwes\app\exceptions\FileException;
use dwes\app\exceptions\ValidationException;
use dwes\app\repository\CategoriaRepository;
use dwes\app\repository\CommentsRepository;
use dwes\app\repository\RestaurantRepository;
use dwes\app\repository\UserRepository;
use dwes\app\utils\File;
use dwes\core\App;
use dwes\app\entity\Restaurant;
use dwes\app\exceptions\QueryException;
use dwes\core\helpers\FlashMessage;
use dwes\core\Response;

/**
 * Created by PhpStorm.
 * User: debian
 * Date: 29/11/18
 * Time: 18:16
 */

class RestaurantController
{
    public  function listar()
    {
        $parameters = [];
        $parameters['active'] = true;
        $url ="/home?";
        if($_SERVER['REQUEST_METHOD']==='GET'){
            $pageSelected = $_GET['page'] ?? 1;
            $nameSearch = $_GET['name'] ?? '';
            $categorySearch = $_GET['categoria'] ?? '';
            $starSearch = $_GET['numberStar'] ?? 0;
            $onlyOpen = $_GET['open'] ?? false;
        }

        if($onlyOpen!=false){
            $date = time();
            $dayofweek = date('w', strtotime($date));
            $dayWeek =  ['Thursday','Monday','Tuesday','Wednesday','Sunday','Friday','Saturday'];
            $parameters['openDays']='%'.$dayWeek[$dayofweek].'%';
        }


        //guardando los parametros

        if($nameSearch!=='' )$parameters['name']=$nameSearch;
        if($categorySearch!=='')$parameters['categoria']=$categorySearch;
        if($starSearch!=0)$parameters['numberStar']=$starSearch;

        //CREANDO LA URL
        $itemCount = 0;
        foreach ($parameters as $key=>$value){
                $url.=$key."=".$value."&";
        }
        $url.="page=";

        $restaurants = [];
        $queryBuilder = App::getRepository(RestaurantRepository::class);
        $queryBuilderUser = App::getRepository(UserRepository::class);
        $categorias = App::getRepository(CategoriaRepository::class)->findAll();
        $totalRestaurants = $queryBuilder->count($parameters);
        $result_per_page =  5;
        $number_of_pages = ceil($totalRestaurants/$result_per_page);

        $this_page_first_result = ($pageSelected-1)*$result_per_page;

        try {
            $restaurants = $queryBuilder->getElementLimit($this_page_first_result, $result_per_page,$parameters);
        }
        catch (\Exception $a){
                echo 'Error '.' '.$a->getMessage();
        }
        Response::renderView('index','layout',compact('queryBuilder','queryBuilderUser','restaurants','number_of_pages',
            'pageSelected','url','categorias','nameSearch','categorySearch','starSearch','onlyOpen'));
    }

    public function addCommentToRestaurante($idRestaurante){
        $nombreCampo = ['comment','rating'];
        $errores =[];
        $mensaje = "";
        $test_input= function ($data,$index) use ($nombreCampo) {
            if(empty($data))
                throw  new ValidationException("Please complete input " . $nombreCampo[$index]);
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        };

        $usuarioId = App::get('appUser')->getId();
        $text = $test_input($_POST['comment']??'','');
        $rating = $test_input($_POST['rating']??'','');
        $date =  date('m/d/Y', time()) ."";

        $c_Comment = new Comment($usuarioId,$idRestaurante,$date,$rating,$text);
        $queryBuilder = App::getRepository(RestaurantRepository::class);
        try {
            $queryBuilder->updateRatingRestaurant($idRestaurante,$c_Comment);
        }
        catch (\Exception $e){
            $errores[]="Error al publicar comentario";
        }
        finally{
            if(empty($errores))
                $mensaje ="Tu comentario ha sido publicado";

            $this->showRestaurant($idRestaurante,$errores,$mensaje);
        }
    }


    /**
     * @param $id
     * @throws AppException
     */
    public function  showRestaurant($id,$errores=[],$mensaje=""){
         $queryBuilder = App::getRepository(RestaurantRepository::class);
         $restaurant = $queryBuilder->find($id);
         $queryBuilderComments = App::getRepository(CommentsRepository::class);
         $queryBuilderUsuario = App::getRepository(UserRepository::class);
         $comments = $queryBuilderComments->getAllCommentsByRestaurante($id);
         Response::renderView('restaurant-details','layout',compact('queryBuilder','restaurant',
         'queryBuilderComments','queryBuilderUsuario','comments','errores','mensaje'    ));
    }

    public function deleteRestaurant($id){
        if (isset($id))
        {
            try {
                App::getRepository(RestaurantRepository::class)->delete($id);
                $resultado = true;
            } catch (QueryException $e) {
                $resultado = false;
            }
        }
        else
            $resultado = false;

        echo json_encode($resultado);
    }



    public function editRestaurant($id){
        $restaurant = App::getRepository(RestaurantRepository::class)->find($id);
        $queryBuilderCategorias = App::getRepository(CategoriaRepository::class) ?? '';
        $categorias = $queryBuilderCategorias->findAll()?? '';
        $errores = FlashMessage::get('errores');
        $mensaje = FlashMessage::get('message');
        $update = true;
        Response::renderView('add-restaurant','layout',
            compact('queryBuilderCategorias', 'categorias', 'errores','restaurant','mensaje','update')
        );
    }

    /**
     * @throws AppException
     * @throws FileException
     * @throws ValidationException
     */
    public function updateRestaurant($id){

        $lastImage = $_POST['lastImagen'];
        $errores = false;
        $queryBuilder = App::getRepository(RestaurantRepository::class);
        $restaurante='';
        //Gestion de Imagen
        try
        {
            $restaurante = $this->CreateRestaurant();
            $restaurante->setId($id);
            $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
            $imagen = new File('image', $tiposAceptados);
            $imagen->saveUploadFile(Restaurant::RUTA_IMAGENES_PORTAFOLIO);
            $restaurante->setImagen($imagen->getFileName());
        }
        catch (ValidationException $validationException) {
            FlashMessage::set('errores',[ $validationException->getMessage()]);
            $errores = true;
        }
        catch (FileException $fileException){
            $restaurante->setImagen($lastImage);
        }
        catch (\Exception $e){
            FlashMessage::set('errores',[ $e->getMessage()]);
            $errores = true;
        }
        finally{
            if(!$errores) {
                $queryBuilder->update($restaurante);
                FlashMessage::set('message','Cambio realizado');
            }
        }
        App::get('router')->redirect('edit-restaurant/'.$id);
    }


    public function  addRestaurant(){

        $errores = FlashMessage::get('errores');
        $mensaje = FlashMessage::get('message');

        $nameInput = FlashMessage::get('name') ;
        $descriptionInput = FlashMessage::get('description') ;
        $cuisineInput = FlashMessage::get('cuisine') ;
        $phoneInput = FlashMessage::get('phone') ;
        $categoriaSeleccionada = FlashMessage::get('categoria');
        $categoriaSeleccionada = $categoriaSeleccionada===''?0:$categoriaSeleccionada;
        $update = false;
        $restaurant = new Restaurant($nameInput,$descriptionInput,"",$phoneInput,$cuisineInput,"","","",
            "",$categoriaSeleccionada);
        $categorias = [];
        try {
            $queryBuilderCategorias = App::getRepository(CategoriaRepository::class) ?? '';
            $categorias = $queryBuilderCategorias->findAll()?? '';
        } catch (AppException $e) {
        } catch (QueryException $e) {
        }
        Response::renderView('add-restaurant','layout',
            compact('queryBuilderCategorias', 'categorias', 'errores','restaurant','mensaje','update')
        );

    }


    public function  newRestaurant(){

        $mensajeConfirmacion = "Su restaurante se ha subido correctamente";
        $errores =false;
        try{
            $queryBuilder = App::getRepository(RestaurantRepository::class);
            $restaurante = $this->CreateRestaurant();

            //Gestion de Imagen
            $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
            $imagen = new File('image', $tiposAceptados);
            $imagen->saveUploadFile(Restaurant::RUTA_IMAGENES_PORTAFOLIO);
            $restaurante->setImagen($imagen->getFileName());
            $queryBuilder->guarda($restaurante);
            $message = 'Se ha añadido una nuevo restaurante: '.$restaurante->getName();
            FlashMessage::set('mensaje',$message);
            $email= App::get("appUser")->getEmail();

            App::get('log')->add($message);
            App::get('mailer')->sendMessage($message,$email);
        }
        catch (ValidationException $validationException) {
            FlashMessage::set('errores',[ $validationException->getMessage()]);
            $errores = true;
        }
        catch (FileException $fileException){
            FlashMessage::set('errores',[ $fileException->getMessage()]);
            $errores = true;
        }
        catch( AppException $appException){
            FlashMessage::set('errores',[ $appException->getMessage()]);
            $errores = true;
        }
        catch (QueryException $queryException){
            FlashMessage::set('errores',[ $queryException->getMessage()]);
            $errores = true;
        }
        catch (\Exception $exception){
            FlashMessage::set('errores',[ $exception->getMessage()]);
            $errores = true;
        }

        if(!$errores)
            try {
                FlashMessage::unsetMessage('errores');
                FlashMessage::unsetMessage('mensaje');
                FlashMessage::unsetMessage('name');
                FlashMessage::unsetMessage('description');
                FlashMessage::unsetMessage('cuisine');
                FlashMessage::unsetMessage('phone');
                FlashMessage::unsetMessage('categoria');

                App::get('router')->redirect('');
            } catch (AppException $e) {
            }
        else
            try {
                App::get('router')->redirect('add-restaurant');
            } catch (AppException $e) {
            }
    }

    /**
     * @return Restaurant
     * @throws AppException
     * @throws FileException
     * @throws ValidationException
     */
    public function CreateRestaurant(): Restaurant
    {

        $diasNombre = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
        $test_input= function ($data,$index)  {

            if(empty($data))
                throw  new ValidationException("Please complete input " . $index);
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        };

        //guardamos los valores
        foreach ($_POST as $key => $value)
            FlashMessage::set($key, $value);

        //validamos y guardamos los valores
        $nombre = $test_input($_POST['name'] ?? '', 'name');
        $descripcion = $test_input($_POST['description'] ?? '', 'description');
        $receta = $test_input($_POST['cuisine'] ?? '', 'cuisine');
        $telefono = $test_input($_POST['phone'] ?? '', 'phone');
        $categoria = $test_input($_POST['categoria'] ?? '', 'category');

        $dias = $_POST['days'] ?? [];
        if (empty($dias))
            throw new ValidationException('Señala los dias que esta abierto el Restaurante');
        else {
            $checked = array();
            foreach ($dias as $animal) {
                $checked[] = $diasNombre[$animal];
            }
        }
        $diasAbierto = implode(",", $checked);
        $fecha = date('m/d/Y', time()) . "";
        $usuarioId = App::get('appUser')->getId();

        $restaurante = new Restaurant($nombre, $descripcion, $diasAbierto,
            $telefono, $receta,"", 0, $fecha, $usuarioId, $categoria);
        return  $restaurante;
    }
}