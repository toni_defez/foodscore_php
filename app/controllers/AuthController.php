<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 13/12/18
 * Time: 19:11
 */

namespace dwes\app\controllers;


use DateTime;
use dwes\app\entity\Usuario;
use dwes\app\exceptions\AppException;
use dwes\app\exceptions\FileException;
use dwes\app\exceptions\QueryException;
use dwes\app\exceptions\ValidationException;
use dwes\app\repository\UserRepository;
use dwes\app\utils\File;
use dwes\core\App;
use dwes\core\helpers\FlashMessage;
use dwes\core\Response;
use dwes\core\Security;

class AuthController
{

    public function  login(){
        $errores = FlashMessage::get('login-error',[]);
        $email = FlashMessage::get('email');
        Response::renderView('login','layout',compact('errores','email'));
    }

    /**
     * @throws \dwes\app\exceptions\AppException
     */
    public function checkLogin(){

        try{
        if(!isset($_POST['email']) || empty($_POST['email']))
        {
            throw new ValidationException("Debes introducir password y usuario");
        }
        FlashMessage::set('email',$_POST['email']);
        if(!isset($_POST['password']) || empty($_POST['password']) )
            throw new ValidationException("Debes introducir password y usuario");

        $usuario = App::getRepository(UserRepository::class)->findBy([
            'email'=>$_POST['email'] ?? '',
        ]);

        if(!is_null($usuario) && !empty($usuario) && Security::checkPassword($_POST['password'],$usuario[0]->getPassword())){
            $_SESSION['loguedUser']=$usuario[0]->getId();
            $_SESSION['idioma'] = $usuario[0]->getIdioma();

            FlashMessage::unsetMessage('email');
            App::get('router')->redirect('');
        }
        throw new ValidationException("Usuario o password incorrectos");
        }
        catch (ValidationException $validationException)
        {
            FlashMessage::set('login-error',[$validationException->getMessage()]);
            App::get('router')->redirect('login');
        }

    }

    /**
     * @throws \dwes\app\exceptions\AppException
     */
    public function logout(){

        if(isset($_SESSION['loguedUser']))
        $_SESSION['loguedUser'] = null;
        unset($_SESSION['loguedUser']);

        App::get('router')->redirect('');
    }

    public function unauthorized()
    {
       header(
           'HTTP/1.1 403 Forbidden',
           true,403);Response::renderView('403');
    }


    /**
     * @throws \dwes\app\exceptions\AppException
     */
    public function registro(){

        $errores = FlashMessage::get('registro-error',[]);
        $mensaje = FlashMessage::get('mensaje');
        $nombre = FlashMessage::get('nameUser');
        $email = FlashMessage::get('email');
        $email2 = FlashMessage::get('email2');
        $birthday = FlashMessage::get('birthday');
        $languaje = FlashMessage::get('languaje');

        Response::renderView('register','layout',compact('errores','mensaje',
            'nombre','email','email2','birthday','languaje'));
    }


    public function  checkRegistro()
    {
        $reviewFields=function ($nombre,$email,$email2,$password,$birthday) {

            if(!empty($nombre) && !empty($email) && !empty($email2) && !empty($password)
                && !empty($birthday))
            {

                if( filter_var($email,FILTER_VALIDATE_EMAIL)=== false or
                    filter_var($email2,FILTER_VALIDATE_EMAIL)===false){

                    throw new ValidationException("Los dos email deben de ser validos");
                }
                else if ( $email !== $email2){

                    throw  new ValidationException("Los dos email deben ser iguales");
                }
                else if(DateTime::createFromFormat('Y-m-d',$birthday)===false){

                    throw  new ValidationException("La fecha no es correcta");
                }
            }
            else
            {
                throw  new ValidationException("Alguno de los campos es vacio");
            }
        };


        FlashMessage::set('nameUser',$_POST['nameUser']);
        FlashMessage::set('email',$_POST['email']);
        FlashMessage::set('email2',$_POST['email2']);
        FlashMessage::set('birthday',$_POST['birthday']);
        FlashMessage::set('languaje',$_POST['languaje']);

        $nombre = trim( htmlspecialchars($_POST['nameUser']));
        $email = trim( htmlspecialchars($_POST['email']));
        $email2 = trim(htmlspecialchars($_POST['email2']));
        $password = trim(htmlspecialchars($_POST['password']));
        $birthday = trim(htmlspecialchars($_POST['birthday']));
        $password1 = Security::encrypt($password);
        $languaje = trim(htmlspecialchars($_POST['languaje']));
        try {
            $reviewFields($nombre,$email,$email2,$password,$birthday);
            $usuario = new Usuario();
            $usuario->setName($nombre);
            $usuario->setEmail($email);
            $usuario->setPassword($password1);
            $usuario->setRol('ROLE_USER');
            $usuario->setBirthday($birthday);
            $usuario->setIdioma($languaje);
            $tiposAceptados = ['image/jpeg', 'image/png', 'image/gif'];
            try
            {
                $imagen = new File('image', $tiposAceptados);
                $imagen->saveUploadFile(Usuario::RUTA_IMAGENES_USUARIOS);
                $usuario->setAvatar($imagen->getFileName());
            }
            catch (FileException $exception) {
                throw new ValidationException($exception->getMessage());
            }
            $queryBuilder = App::getRepository(UserRepository::class);
            $queryBuilder->save($usuario);
            FlashMessage::unsetMessage('nameUser');
            FlashMessage::unsetMessage('email');
            FlashMessage::unsetMessage('email2');
            FlashMessage::unsetMessage('birthday');
            FlashMessage::unsetMessage('languaje');
            App::get('router')->redirect('login');
        }
        catch (ValidationException $validationException) {
            FlashMessage::set('registro-error',[$validationException->getMessage()]);
            App::get('router')->redirect('register');
        } catch (AppException $e) {
        }
        catch (QueryException $queryException){
            FlashMessage::set('registro-error',[$queryException->getMessage()]);
            App::get('router')->redirect('register');
        }
    }
}
