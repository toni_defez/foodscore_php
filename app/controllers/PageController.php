<?php

namespace  dwes\app\controllers;

use dwes\app\exceptions\QueryException;
use dwes\app\repository\RestaurantRepository;
use dwes\core\App;
use dwes\core\Response;

/**
 * Created by PhpStorm.
 * User: debian
 * Date: 29/11/18
 * Time: 18:02
 */

class PageController
{

    public function about(){
        Response::renderView('about','layout');
    }

}