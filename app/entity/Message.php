<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 1/01/19
 * Time: 17:52
 */

namespace dwes\app\entity;


use dwes\core\database\IEntity;

class Message implements  IEntity
{

    private $id;

    /**
     * @var int
     */
    private $remitente_id;
    /**
     * @var int
     */
    private $destinatario_id;
    /**
     * @var string
     */
    private $titulo;

    /**
     * @var string
     */
    private $mensaje;

    private $fecha_hora;

    /**
     * Message constructor.
     * @param string $remitente_id
     * @param string $destinatario_id
     * @param string $titulo
     */
    public function __construct(int $remitente_id=0, int $destinatario_id=0,string $titulo=""
    ,string $mensaje="")
    {
        $this->remitente_id = $remitente_id;
        $this->destinatario_id = $destinatario_id;
        $this->titulo = $titulo;
        $this->mensaje = $mensaje;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Message
     */
    public function setId(int $id): Message
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getRemitenteId(): int
    {
        return $this->remitente_id;
    }

    /**
     * @param int $remitente_id
     * @return Message
     */
    public function setRemitenteId(int $remitente_id): Message
    {
        $this->remitente_id = $remitente_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getDestinatarioId(): int
    {
        return $this->destinatario_id;
    }

    /**
     * @param int $destinatario_id
     * @return Message
     */
    public function setDestinatarioId(int $destinatario_id): Message
    {
        $this->destinatario_id = $destinatario_id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitulo(): string
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return Message
     */
    public function setTitulo(string $titulo): Message
    {
        $this->titulo = $titulo;
        return $this;
    }

    /**
     * @return string
     */
    public function getMensaje(): string
    {
        return $this->mensaje;
    }

    /**
     * @param string $mensaje
     * @return Message
     */
    public function setMensaje(string $mensaje): Message
    {
        $this->mensaje = $mensaje;
        return $this;
    }


    public function getFechaHora()
    {
        return $this->fecha_hora;
    }

    /**
     * @param string $fecha_hora
     * @return Message
     */
    public function setFechaHora(string $fecha_hora): Message
    {
        $this->fecha_hora = $fecha_hora;
        return $this;
    }


    public function toArray(): array
    {
        return [
            'id'=>$this->getId(),
            'remitente_id'=>$this->getRemitenteId(),
            'destinatario_id'=>$this->getDestinatarioId(),
            'titulo'=>$this->getTitulo(),
            'mensaje'=>$this->getMensaje(),
            'fecha_hora'=>$this->getFechaHora()
        ];
    }
}