<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 12/11/18
 * Time: 18:19
 */
namespace  dwes\app\entity;

use dwes\core\database\IEntity;

class Usuario implements  IEntity
{

    CONST RUTA_IMAGENES_USUARIOS ='./images/users/';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $avatar;

    /**
     * @var string
     */
    private $rol;

    /**
     * @var int
     */
    private $num_restaurants;


    /**
     * @var string
     */
    private $birthday;

    /*
     * @var string
     */
    private $idioma;

    /**
     * Usuario constructor.
     * @param string $name
     * @param string $email
     * @param string $password
     * @param string $avatar
     * @param string $rol
     * @param int $num_restaurants
     * @param string $birthday
     */
    public function __construct(  $name ='',
                                  $email ='',  $password='',  $avatar='',  $rol='',
                                  $num_restaurants=0, $birthday ='',$idioma ='')
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->avatar = $avatar;
        $this->rol = $rol;
        $this->num_restaurants = $num_restaurants;
        $this->birthday = $birthday;
        $this->idioma = $idioma;
    }

    /**
     * @return string
     */
    public function getIdioma(): string
    {
        return $this->idioma;
    }

    /**
     * @param string $idioma
     * @return Usuario
     */
    public function setIdioma(string $idioma): Usuario
    {
        $this->idioma = $idioma;
        return $this;
    }



    /**
     * @return string
     */
    public function getBirthday(): string
    {
        return $this->birthday;
    }

    /**
     * @param string $birthday
     * @return Usuario
     */
    public function setBirthday(string $birthday): Usuario
    {
        $this->birthday = $birthday;
        return $this;
    }



    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Usuario
     */
    public function setId(int $id): Usuario
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Usuario
     */
    public function setName(string $name): Usuario
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Usuario
     */
    public function setEmail(string $email): Usuario
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Usuario
     */
    public function setPassword(string $password): Usuario
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar(): string
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     * @return Usuario
     */
    public function setAvatar(string $avatar): Usuario
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getRol(): string
    {
        return $this->rol;
    }

    /**
     * @param string $rol
     * @return Usuario
     */
    public function setRol(string $rol): Usuario
    {
        $this->rol = $rol;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumRestaurants(): int
    {
        return $this->num_restaurants;
    }

    /**
     * @param int $num_restaurants
     * @return Usuario
     */
    public function setNumRestaurants( $num_restaurants): Usuario
    {
        $this->num_restaurants = $num_restaurants;
        return $this;
    }

    public function getUrlAvatar() : string
    {
        return self::RUTA_IMAGENES_USUARIOS . $this->getAvatar();
    }

    public function getUrlMiniaturaAvatar():string
    {
        return self::RUTA_IMAGENES_USUARIOS ."min_". $this->getAvatar();
    }


    public function toArray(): array
    {
      return [
          'id'=> $this->getId(),
          'name'=>$this->getName(),
          'email'=>$this->getEmail(),
          'password'=>$this->getPassword(),
          'avatar'=>$this->getAvatar(),
          'rol'=>$this->getRol(),
          'num_restaurants'=>$this->getNumRestaurants(),
          'birthday'=>$this->getBirthday(),
          'idioma'=>$this->getIdioma()
      ];
    }
}