<?php
namespace  dwes\app\entity;

use dwes\app\repository\RestaurantRepository;
use dwes\core\App;
use dwes\core\database\IEntity;


class Restaurant implements  IEntity
{
    CONST RUTA_IMAGENES_PORTAFOLIO ='./images/restaurants/';
    CONST RUTA_IMAGENES_GALLERY ='./images/';

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $openDays;

    /**
     * @var string
     */
    private $telephone;

    /**
     * @var string
     */
    private $cuisine;

    /**
     * @var string
     */
    private $imagen;

    /**
     * @var int
     */
    private $numberStar;

    /**
     * @var string
     */
    private $date;

    /**
     * @var
     */
    private $userId;


    /**
     * @var int
     */
    private $categoria;

    /**
     * @var bool
     */
    private $active;

    /**
     * Restaurant constructor.
     * @param string $name
     * @param string $description
     * @param string $openDays
     * @param string $telephone
     * @param string $cuisine
     * @param string $imagen
     * @param int $numberStar
     * @param string $dateInit
     * @param int $userId
     */
    public function __construct($name="", $description="",
                                 $openDays='',  $telephone="",
                                 $cuisine='', $imagen="", $numberStar=0, $date='',
                                $userId=1, $categoria = 0)
    {
        $this->name = $name;
        $this->description = $description;
        $this->openDays = $openDays;
        $this->telephone = $telephone;
        $this->cuisine = $cuisine;
        $this->imagen = $imagen;
        $this->numberStar = $numberStar;
        $this->date = $date;
        $this->userId=$userId;
        $this->categoria = $categoria;
        $this->active = true;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return Restaurant
     */
    public function setActive(bool $active): Restaurant
    {
        $this->active = $active;
        return $this;
    }



    public function setId($id){
        $this->id=$id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Restaurant
     */
    public function setName(string $name): Restaurant
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Restaurant
     */
    public function setDescription(string $description): Restaurant
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getOpenDays(): string
    {
        return $this->openDays;
    }

    /**
     * @param string $openDays
     * @return Restaurant
     */
    public function setOpenDays(string $openDays): Restaurant
    {
        $this->openDays = $openDays;
        return $this;
    }

    /**
     * @return string
     */
    public function getTelephone(): string
    {
        return $this->telephone;
    }

    /**
     * @param string $telephone
     * @return Restaurant
     */
    public function setTelephone(string $telephone): Restaurant
    {
        $this->telephone = $telephone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCuisine(): string
    {
        return $this->cuisine;
    }

    /**
     * @param string $cuisine
     * @return Restaurant
     */
    public function setCuisine(string $cuisine): Restaurant
    {
        $this->cuisine = $cuisine;
        return $this;
    }

    /**
     * @return string
     */
    public function getImagen(): string
    {
        return $this->imagen;
    }

    /**
     * @param string $imagen
     * @return Restaurant
     */
    public function setImagen(string $imagen): Restaurant
    {
        $this->imagen = $imagen;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumberStar(): int
    {
        return $this->numberStar;
    }

    /**
     * @param int $numberStar
     * @return Restaurant
     */
    public function setNumberStar(int $numberStar): Restaurant
    {
        $this->numberStar = $numberStar;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getuserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     * @return Restaurant
     */
    public function setuserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }
    
    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    public function getUrlPortafolio() : string
    {
        return self::RUTA_IMAGENES_PORTAFOLIO . $this->getImagen();
    }

    public function getUrlMiniatura():string
    {
        return self::RUTA_IMAGENES_PORTAFOLIO."min_".$this->getImagen();
    }

    public function getUrlImagen():string
    {
        return self::RUTA_IMAGENES_GALLERY . $this->getName();
    }

    /**
     * @return int
     */
    public function getCategoria(): int
    {
        return $this->categoria;
    }

    /**
     * @param int $categoria
     * @return Restaurant
     */
    public function setCategoria(int $categoria): Restaurant
    {
        $this->categoria = $categoria;
        return $this;
    }

    public function checkDayOpen(string $day):bool{

        $result=  strpos($this->getOpenDays(),$day)!== false;
        return $result;
    }

    public function isOpen():bool {
        $date = round(microtime(true) * 1000);
        $dayofweek = date('w', strtotime($date));
        $dayWeek =  ['Thursday','Monday','Tuesday','Wednesday','Sunday','Friday','Saturday'];

        return  strpos($this->getOpenDays(), $dayWeek[$dayofweek])!== false;
    }

    public function isMine():bool{
        $user = App::get('appUser')??"";
        if($user == "")return false;
        return $user->getId()  == $this->getuserId();
    }



    public function toArray(): array
    {
        return [
            'id'=>$this->getId(),
            'name'=>$this->getName(),
            'description'=>$this->getDescription(),
            'date'=>$this->getDate(),
            'openDays'=>$this->getOpenDays(),
            'telephone'=>$this->getTelephone(),
            'cuisine'=>$this->getCuisine(),
            'imagen'=>$this->getImagen(),
            'numberStar'=>$this->getNumberStar(),
            'userId'=>$this->getuserId(),
            'categoria'=>$this->getCategoria(),
            'active'=>$this->isActive()
        ];
    }
}