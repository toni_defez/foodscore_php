<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 11/12/18
 * Time: 0:57
 */

namespace dwes\app\entity;


use dwes\core\database\IEntity;

class Comment implements  IEntity
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $id_usuario;

    /**
     * @var int
     */
    private $id_restaurante;

    /**
     * @var string
     */
    private $fecha;

    /**
     * @var int
     */
    private $rating;

    /**
     * @var string
     */
    private $text;

    /**
     * Comment constructor.
     * @param int $id_usuario
     * @param int $id_restaurante
     * @param string $fecha
     * @param int $rating
     * @param string $text
     */
    public function __construct(int $id_usuario=0, int $id_restaurante=0, string $fecha='', int $rating=0, string $text='')
    {
        $this->id_usuario = $id_usuario;
        $this->id_restaurante = $id_restaurante;
        $this->fecha = $fecha;
        $this->rating = $rating;
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Comment
     */
    public function setId(int $id): Comment
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdUsuario(): int
    {
        return $this->id_usuario;
    }

    /**
     * @param int $id_usuario
     * @return Comment
     */
    public function setIdUsuario(int $id_usuario): Comment
    {
        $this->id_usuario = $id_usuario;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdRestaurante(): int
    {
        return $this->id_restaurante;
    }

    /**
     * @param int $id_restaurante
     * @return Comment
     */
    public function setIdRestaurante(int $id_restaurante): Comment
    {
        $this->id_restaurante = $id_restaurante;
        return $this;
    }

    /**
     * @return string
     */
    public function getFecha(): string
    {
        return $this->fecha;
    }

    /**
     * @param string $fecha
     * @return Comment
     */
    public function setFecha(string $fecha): Comment
    {
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * @return int
     */
    public function getRating(): int
    {
        return $this->rating;
    }

    /**
     * @param int $rating
     * @return Comment
     */
    public function setRating(int $rating): Comment
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return Comment
     */
    public function setText(string $text): Comment
    {
        $this->text = $text;
        return $this;
    }


    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id'=>$this->getId(),
            'id_usuario'=>$this->getIdUsuario(),
            'id_restaurante'=>$this->getIdRestaurante(),
            'fecha'=>$this->getFecha(),
            'rating'=>$this->getRating(),
            'text'=>$this->getText()
        ];
    }
}

