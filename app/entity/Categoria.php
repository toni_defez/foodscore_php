<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 9/12/18
 * Time: 16:34
 */

namespace dwes\app\entity;


use dwes\core\database\IEntity;

class Categoria implements  IEntity
{

    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $nombre;
    /**
     * @var int
     */
    private $numRestaurante;

    /**
     * Categoria constructor.
     * @param int $id
     * @param string $nombre
     * @param int $numRestaurante
     */
    public function __construct(  string $nombre="", int $numRestaurante=0)
    {
        $this->id = null;
        $this->nombre = $nombre;
        $this->numRestaurante = $numRestaurante;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Categoria
     */
    public function setId(int $id): Categoria
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Categoria
     */
    public function setNombre(string $nombre): Categoria
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumRestaurante(): int
    {
        return $this->numRestaurante;
    }

    /**
     * @param int $numRestaurante
     * @return Categoria
     */
    public function setNumRestaurante(int $numRestaurante): Categoria
    {
        $this->numRestaurante = $numRestaurante;
        return $this;
    }


    public function toArray(): array
    {
        return [
            'id'=>$this->getId(),
            'nombre'=>$this->getNombre(),
            'numRestaurante'=>$this->getNumRestaurante()
        ];
    }
}