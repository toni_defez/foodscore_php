
<?php
session_start();
if (isset($_GET["language"]))
    $language = trim(strip_tags($_GET["language"]));
else
{
    if (isset($_SESSION["language"]))
        $language = $_SESSION["language"];
    else
        $language = "es_ES";
}
$_SESSION["language"] = $language;

$language .= ".utf8";
$numComentarios = 2;

setlocale(LC_ALL, $language);

bindtextdomain("messages", "../locale");

bind_textdomain_codeset("messages", "UTF-8");

textdomain("messages");

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<p>
    <a href="<?= $_SERVER["PHP_SELF"]; ?>?language=en_GB">
        English</a> -
    <a href="<?= $_SERVER["PHP_SELF"]; ?>?language=es_ES">
        Español</a>
</p>
    <h1><?= _("Hola mundo"); ?></h1>
    <p><?= _('adios') ?></p>
    <p><?= _('ADIOS')?></p>
    <p><?= _('COsas')?></p>

</body>
</html>
