<?php
use dwes\app\exceptions\AppException;
use dwes\app\exceptions\NotFoundException;
use dwes\core\App;
use dwes\core\Request;
use dwes\core\Router;

try {
    require __DIR__.'/../core/bootstrap.php';
    Router::load('../app/routes.php');
    App::get('router')->direct(Request::uri(), Request::method());
}
catch(NotFoundException $exception)
{
    die($exception->getMessage());
}
catch (AppException $exception){
    die($exception->getMessage());
}