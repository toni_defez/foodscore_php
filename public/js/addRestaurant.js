'use strict';

let newPlaceForm = null;


document.addEventListener("DOMContentLoaded", e => {
    newPlaceForm = document.getElementById("newPlace");

    newPlaceForm.image.addEventListener('change', loadImage);

    newPlaceForm.addEventListener('submit', validateForm);
});

function testInputExpr(input, expr) {
    input.classList.remove("is-valid", "is-invalid");
    if (expr.test(input.value)) {
        input.classList.add("is-valid");
        return true;
    } else {
        input.classList.add("is-invalid");
        return false;
    }
}

function validatePhone() {
    return testInputExpr(document.getElementById("phone"), /[0-9]+(\.[0-9]{1,2})?/);
}

function validateName() {
    return testInputExpr(document.getElementById("name"), /[a-z][a-z ]*/);
}

function validateDescription() {
    return testInputExpr(document.getElementById("description"), /.*\S.*/);
}

function validateCuisine() {
    return testInputExpr(document.getElementById("cuisine"), /.*\S.*/);
}

function validateDays(daysArray) {
    let daysError = document.getElementById('daysError');
    if(!daysArray.length)
        daysError.classList.remove('d-none');
    else
        daysError.classList.add('d-none');
    return daysArray.length > 0;
}

function validateImage() {
    let imgInput = document.getElementById("image");

    imgInput.classList.remove("is-valid", "is-invalid");
    if(imgInput.files.length > 0) {
        imgInput.classList.add("is-valid");
        return true;
    } else {
        imgInput.classList.add("is-invalid");
        return false;
    }
}

function validateForm(event) {

    /*
    event.preventDefault();
    let name = newPlaceForm.name.value;
    let image = document.getElementById("imgPreview").src;
    let cuisine = newPlaceForm.cuisine.value;
    let description = newPlaceForm.description.value;
    let phone = newPlaceForm.phone.value;


    if (validateName() && validateDescription() && validateCuisine() &&
         validatePhone() && validateImage()) {
        newPlaceForm.submit();
    }
    */

}

function loadImage(event) {
    let file = event.target.files[0];
    let reader = new FileReader();

    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        document.getElementById("imgPreview").src = reader.result;
    });
}
