
let UserDetail = null;
let profileform = null;
let avatarform = null;
let passwordform = null;
let avatar = null;
let imgPreview = null;

window.addEventListener('DOMContentLoaded', async () => {

    profileform =document.getElementById("form-profile");
    avatarform = document.getElementById("form-avatar");
    passwordform = document.getElementById("form-password");
    avatar =document.getElementById("avatar");
    imgPreview =document.getElementById("imgPreview");
    avatarform.addEventListener('submit', updateAvatar);
    avatarform.image.addEventListener('change', loadImage);
});

async function updateAvatar(event) {
    event.preventDefault();
    let image = (document.getElementById("imgPreview"));
    if (image.src != "") {
        image.src = "";
        image.classList.add("d-none");
        avatarform.submit();
    }
}

function loadImage(event) {
    let file = event.target.files[0];
    let reader = new FileReader();
    let elementImage = (document.getElementById("imgPreview"));
    elementImage.classList.remove("d-none");
    if (file) reader.readAsDataURL(file);

    reader.addEventListener('load', e => {
        elementImage.src =
            reader.result.toString();
    });
}
