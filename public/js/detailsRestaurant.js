'use strict';

let container = null;
let restaurant = null;
let comments = null;
let containerComments= null;
let newCommentForm = null;
let rating = null;

let stars= null;
let score =0;

window.addEventListener('DOMContentLoaded', async () => {

    rating = document.getElementById("rating");
    newCommentForm = document.getElementById("commentForm");
    newCommentForm.addEventListener('submit', validateForm);
    container = document.getElementById('cardContainer');
    containerComments = document.getElementById('comments');
    let containerStars = document.getElementById('stars');
    stars = Array.from(containerStars.children);

    stars.forEach(element => {
        element.addEventListener("click",e=>{
            score = +element.dataset['score'];
            rating.value = score;
            dibujar();
        })
    });

});



function dibujar(){
    stars.forEach(element => {
        element.classList.remove("fas", "far");
        let currentScore = +element.dataset['score'];
        if(currentScore<=score)
            element.classList.add("fas");
        else
            element.classList.add("far");
    });
}

async function validateForm(event) {
    event.preventDefault();
    let text1  = document.getElementById("comment");

    if(score== 0){
        alert("Please select a star to valorate this restaurant");
    }
    else
    {
        newCommentForm.submit();
    }
}

