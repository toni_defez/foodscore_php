function iniciarEventos()
{
    let arrDeletes = document.querySelectorAll('.btn-delete');

    arrDeletes.forEach(element => {
        element.addEventListener('click', deleteImage);
    });
}

function deleteImage(event)
{

    event.preventDefault();

    let element = event.target;

    let id = element.getAttribute('id');

    let url = 'http://dwes.local/delete-restaurant/' + id;

    fetch(url, {
        method: 'GET',
        headers: {},
        body: null
    }).then(response => {
        if (response.ok)
        {
            element.parentElement.parentElement.remove();
        }
        else
        {
            alert('No se ha podido borrar el elemento');
        }
    })
}

document.addEventListener("DOMContentLoaded", iniciarEventos);
