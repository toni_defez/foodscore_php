<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 10/12/18
 * Time: 23:06
 */

namespace dwes\core;


class Response
{
    /**
     * @param string $name
     * @param string $layout
     * @param array $data
     * @throws \dwes\app\exceptions\AppException
     */
    public static function renderView (string $name,string $layout ='layout',array $data = []){

        extract($data);

        $app['user'] = App::get('appUser');
        ob_start();

        require __DIR__ . "/../app/views/$name.view.php";

        $mainContent = ob_get_clean();

        require  __DIR__. "/../app/views/$layout.view.php";

    }
}