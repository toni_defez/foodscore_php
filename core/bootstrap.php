<?php

use dwes\app\controllers\InternacionalizationController;
use dwes\app\repository\UserRepository;
use dwes\core\App;
use dwes\core\Router;
use dwes\app\exceptions\AppException;
use dwes\app\utils\MyLog;
use dwes\app\utils\MyMailer;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


session_start();
require __DIR__.'/../vendor/autoload.php';
//conexion a la base de datos

$config = require_once __DIR__.'/../app/config.php';
App::bind('config',$config);

try {
    $connection = App::getConnection();
} catch (AppException $e) {
}

$logger = MyLog::load();
App::bind('log',$logger);

/*** Para enviar un nuevo correo ***/
$mailer = MyMailer::load();
App::bind('mailer',$mailer);

if (isset($_SESSION['idioma']))
    $language = $_SESSION['idioma'];
else
{
    $language = App::getRepository(InternacionalizationController::class)->detectarIdioma();
    if($language == "es")
        $language = "es_ES";
    else
        $language = "en_GB";
    $_SESSION['idioma'] = $language;
}

$language .= ".utf8";

setlocale(LC_ALL, $language);

bindtextdomain("messages", "../locale");

bind_textdomain_codeset("messages", "UTF-8");

textdomain("messages");



/** Para guardar el usuario logeado  */
if(isset($_SESSION['loguedUser']))
    $appUser = App::getRepository(UserRepository::class)->find($_SESSION['loguedUser']);
else
    $appUser = null;

App::bind('appUser',$appUser);

