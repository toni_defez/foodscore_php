<?php
namespace  dwes\core\database;

use dwes\app\exceptions\NotFoundException;
use dwes\app\exceptions\QueryException;
use dwes\core\App;
use dwes\exceptions\AppException;

use PDO;
use PDOException;

abstract  class QueryBuilder
{
    /**
     * @var PDO
     */
    protected $connection;
    /**
     * @var string
     */
    protected $table;
    /**
     * @var string
     */
    protected $classEntity;
    /**
     * QueryBuilder constructor.
     * @throws AppException
     */
    public function __construct(string $table,string $classEntity)
    {
        $this->connection = App::getConnection();
        $this->table = $table;
        $this->classEntity = $classEntity;
    }


    /**
     * @param string $sql
     * @return array
     * @throws QueryException
     */
    protected function  executeQuery(string $sql,array $parameters=[]){
        $pdoStatment =$this->connection->prepare($sql);

        if($pdoStatment->execute($parameters) === false){
            throw new QueryException("No se ha podido ejecutar la query solicitada");
        }
        return $pdoStatment->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
    }


    /**
     * @param string $sql
     * @return array
     * @throws QueryException
     */
    protected  function executeQueryArray(string $sql , array $parameters = []){
        $pdoStatment =$this->connection->prepare($sql);

        try{
            if($pdoStatment->execute($parameters) === false){
                throw new QueryException("No se ha podido ejecutar la query solicitada");
            }
        }catch (\Exception $e){
            echo $e->getMessage();
        }

        return $pdoStatment->fetchAll(PDO::FETCH_ASSOC );
    }

    /**
     * @param string $table
     * @param string $classEntity
     * @return array
     * @throws QueryException
     */
    public function findAll() :array
    {
       $sql = "SELECT * FROM $this->table";
       return $this->executeQuery($sql);
    }

    /**
     * @param int $id
     * @return IEntity|null
     * @throws NotFoundException
     * @throws QueryException
     */
    public function  find( int $id):?IEntity{

        $sql = "SELECT * FROM $this->table WHERE id = $id";
        $result = $this->executeQuery($sql);
        if(empty($result))
            throw new NotFoundException("No se ha encontrado ningún elemento");
        else
            return $result[0];
    }

    public function count($parameters =[]):?int{
        if(empty($parameters)) {
            $sql = "SELECT COUNT(*) as total FROM $this->table";
            return $this->executeQueryArray($sql)[0]['total'];
        }
        else{
            $fields = $this->getStrParams($parameters, ' and ');
            $sql = sprintf(
                "SELECT COUNT(*) as total FROM %s WHERE %s",
                $this->table,
                $fields
            );
            return $this->executeQueryArray($sql,$parameters)[0]['total'];
        }
    }

    private function getStrParams(array $parametros, string $conector)
    {
        $fields = [];
        $keys = array_keys($parametros);

        foreach ($keys as $key)
        {
            if($key ==='openDays'){
                $fields[] = $key . ' LIKE :' . $key;
            }
            else
            $fields[] = $key . ' = :' . $key;

        }

        return implode($conector, $fields);
    }

    public function findBy(array $parametros)
    {
        $fields = $this->getStrParams($parametros, ' and ');

        $sql = sprintf(
            "SELECT * FROM %s WHERE %s",
            $this->table,
            $fields
        );

        $pdoStatement = $this->connection->prepare($sql);

        $pdoStatement->execute($parametros);

        return $pdoStatement->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $this->classEntity);
    }

    /**
     * @param IEntity $IEntity
     * @throws QueryException
     */
    public function save(IEntity $IEntity){

        try
        {
            $parameters = $IEntity->toArray();
            $sql = sprintf('insert into %s (%s) values (%s)',
            $this->table,
            implode(', ', array_keys($parameters)),
            ':' . implode(',:', array_keys($parameters))
            );

            $statement = $this->connection->prepare($sql);
            $statement->execute($parameters);
        }
        catch (PDOException $exception){
            throw new QueryException('Error al insertar en la base de datos');
        }
    }

    /**
     * @param int $id
     */
    public function delete(int $id)
    {
       $sql = sprintf(
            "DELETE FROM %s WHERE id=$id",
            $this->table
        );
        $pdoStatement = $this->connection->prepare($sql);
        $pdoStatement->execute();
    }

    public function  executeTransaction(callable  $fnExecuteQuerys){
        try{
            //empiezo bloque
            $this->connection->beginTransaction();
            //sentencias
            $fnExecuteQuerys();
            //todas correctas subida
            $this->connection->commit();
        }
        catch (PDOException $pdoException){
            //tiramos para atras
            $this->connection->rollBack();
            throw new QueryException("No se ha podido realizar la operacion");
        }
    }

    private function  getUpdates(array $parametros){
        $updates = '';
        foreach ($parametros as $key => $value){
            if($key !=='id'){
                if($updates !== '')
                    $updates.=',';
                $updates .=$key . '=:'.$key;
            }
        }
        return $updates;
    }


    /**
     * @param IEntity $entity
     * @throws QueryException
     */
    public function  update (IEntity $entity):void{

        $parametros = $entity->toArray();
        try{
            $sql = sprintf('UPDATE %s SET %s WHERE id=:id',
                $this->table,
                $this->getUpdates($parametros));
            $pdoStatement = $this->connection->prepare($sql);
            $pdoStatement->execute($parametros);
        }
        catch (PDOException $PDOException){
            throw  new QueryException('Error al actualizar el elementos con id '.$parametros['id']);
        }
    }

    /*HAY QUE ENVIARLE PARAMETROS PARA LAS BUSQUEDAS CONCRETAS*/
    public function getElementLimit($this_page_first_result,$result_per_page,$parameters=[]):array{
        if(empty($parameters)) {
            $sql = "SELECT * FROM " . $this->table . " LIMIT " . $this_page_first_result . "," . $result_per_page;
            return $this->executeQuery($sql);
        }
        else{
            $fields = $this->getStrParams($parameters, ' and ');
            $sql = sprintf(
                "SELECT * FROM %s WHERE %s ". " LIMIT " . $this_page_first_result . "," . $result_per_page,
                $this->table,
                $fields
            );
            return $this->executeQuery($sql,$parameters);
        }
    }

}