<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 12/11/18
 * Time: 21:24
 */
namespace  dwes\core\database;

interface IEntity
{
    public function toArray():array ;
}