<?php
namespace  dwes\core\database;

use dwes\core\App;
use dwes\exceptions\AppException;
use PDO;
use PDOException;

class Connection
{

    /**
     * @return PDO
     * @throws AppException
     */
    public static function make()
    {
        try{

            $config = App::get('config')['database'];
            $connection = new PDO(
                $config['connection'].';dbname='.$config['name'],
                 $config['username'],$config['password']
                ,$config['options']);
        }
        catch (PDOException $PDOException)
        {
            throw  new AppException('No se ha creado la conexion a al base de datos');
        } catch (AppException $e) {
        }

        return $connection;
    }
}