<?php
namespace dwes\core;
use dwes\app\exceptions\AuthenticationException;
use dwes\app\exceptions\NotFoundException;
use Exception;

class Router
{
    private $routes = [
        'GET' => [],
        'POST' => []
    ];

    public static function load($file)
    {

        $router = new static;
        require $file;
        App::bind('router', $router);
        return $router;
    }

    public function get(
        string $uri,
        string $controller,
        $role = 'ROLE_ANONYMUS')
    {
        $this->routes['GET'][$uri] =[
            'controller'=>$controller,
            'role'=>$role
        ];
    }

    public function post(
        string $uri,
        string $controller,
        $role = 'ROLE_ANONYMUS')
    {
        $this->routes['POST'][$uri] =[
            'controller'=>$controller,
            'role'=>$role
        ];
    }

    private function prepareRoute(string $route)
    {
        $urlRule = str_replace('/',
            '\/', $route);

        return preg_replace(
            '/:([^\/]+)/',
            '(?<\1>[0-9]+)', // sólo se admiten dígitos como id
            $urlRule
        );
    }


    /**
     * @param string $route
     * @param array $matches
     * @return array
     */
    private function getParametersRoute(string $route, array $matches)
    {
        preg_match_all('/:([^\/]+)/', $route, $parameterNames);

        return array_intersect_key($matches, array_flip($parameterNames[1]));
    }


    /**
     * @param string $controller
     * @param string $action
     * @throws NotFoundExceptio
     */
    private function  callAction( string $controller,string $action, array $parameters=[]) :bool
    {
        try {
            $controller = 'dwes\\app\\controllers\\' . $controller;
            $objController = new $controller();
            if (!method_exists($objController, $action))
                throw new NotFoundException("El controllador $controller no responde al action $action");

            call_user_func_array([$objController, $action], $parameters);
            return true;
        }
        catch (\TypeError $exception) {
            return false;
        }
    }


    /**
     * @param string $uri
     * @param string $method
     * @throws Exception
     */
    public function direct(string $uri, string $method):void
    {
        foreach($this->routes[$method] as $route=>$routeData)
        {
            $controller = $routeData['controller'];
            $minRole = $routeData['role'];
            $urlRule = $this->prepareRoute($route);

            if (preg_match('/^' . $urlRule . '\/*$/s', $uri, $matches))
            {
                if(Security::isUserGranted($minRole) === false){
                    if(!is_null(App::get('appUser')))
                        $this->callAction('AuthController','unauthorized');
                    else{
                        $this->redirect('login');
                    }
                }
                else{
                    $parameters = $this->getParametersRoute($route, $matches);
                    list($controller, $action) = explode('@', $controller);
                    if ($this->callAction($controller, $action, $parameters) === true)
                        return;
                }
            }
        }
        throw new Exception('No se ha definido una ruta para esta URI');
    }

    public function redirect(string $path)
    {
        header('location: /' . $path);
        exit();
    }



}