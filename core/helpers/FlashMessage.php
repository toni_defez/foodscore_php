<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 12/12/18
 * Time: 23:53
 */

namespace dwes\core\helpers;

class FlashMessage
{
    public static function get(
        string $key, $default = '')
    {
        $value = $default;

        if (isset($_SESSION['flash-message']))
        {
            $value = $_SESSION['flash-message'][$key] ?? $default;

            $_SESSION['flash-message'][$key] = NULL;

            unset($_SESSION['flash-message'][$key]);
        }

        return $value;
    }

    public static function set(string $key, $value)
    {
        $_SESSION['flash-message'][$key] = $value;
    }

    public static function unsetMessage(string $key)
    {
        if (isset($_SESSION['flash-message']))
            unset($_SESSION['flash-message'][$key]);
    }
}