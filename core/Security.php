<?php
/**
 * Created by PhpStorm.
 * User: debian
 * Date: 13/12/18
 * Time: 23:08
 */
namespace dwes\core;

class Security
{
    private static function getRoleNumber(
        string $roleBuscado, array $roles) : int
    {
        foreach($roles as $role=>$valor)
        {
            if ($roleBuscado === $role)
                return $valor;
        }

        return -1;
    }



    /**
     * @param string $role
     * @return bool
     * @throws \dwes\app\exceptions\AppException
     */
    public static function isUserGranted(string $role):bool
    {
        if($role ==='ROLE_ANONYMUS')
            return true;

        $usuario = \dwes\core\App::get('appUser');
        if(is_null($usuario) === true)
            return false;

        $valor_role = \dwes\core\App::get('config')['security']['roles'][$role];
        $rol_usuario = $usuario->getRol();
        $valor_role_usuario = \dwes\core\App::get('config')['security']['roles'][$rol_usuario];

        return $valor_role_usuario >= $valor_role;
    }

    /**
     * @param string $password
     * @return string
     */
    public static function encrypt (string $password):string
    {
        return password_hash($password,PASSWORD_BCRYPT);
    }

    /**
     * @param string $password
     * @param string $bdPassword
     * @return bool
     */
    public static function checkPassword(string $password,string $bdPassword):bool{
        return password_verify($password,$bdPassword);
    }

}